import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormControl} from '@angular/forms';

import { OrganizationService } from './service/organization.service';
import { PlannerService } from './service/planner.service';
import { AdministrationService } from './service/administration.service';
import { ParamService } from './service/param.service';
import { DialogService } from './service/dialog.service';

import { Administration } from './base/administration';
import { Accommodation } from './base/accommodation';
import { Booking } from './base/booking';
import { Activity } from './base/activity';
import { RoomDetail, RoomTime, RoomGroup } from './base/room';
import { Time } from './base/time';
import { User, Relation } from './base/relation';
import { DateService } from './service/date.service';
import { BATCH, BatchService } from './service/batch.service';

import { IntervalObservable } from "rxjs/observable/IntervalObservable";
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDatepickerInputEvent} from '@angular/material/datepicker';
import { MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatSlideToggle } from '@angular/material/slide-toggle';

import { Observable} from 'rxjs';
import { startWith,  map} from 'rxjs/operators';

import { DialogToevoegen }       from './dialog-toevoegen';
import { DialogWijzigen }        from './dialog-wijzigen';
import { DialogVerplaatsen }     from './dialog-verplaatsen';

import { MY_FORMATS } from './const';

export const enum_planner_open = 1;
export const enum_planner_blockbooking = 2;
export const enum_planner_closed = 3;
export const enum_planner_prime = 4;
export const enum_planner_happyhour = 6;
export const enum_planner_booking = 7;

const OPTION_MOVE = 1;
const OPTION_COPY = 2;
const OPTION_DELETE = 3;
const OPTION_ARRIVE = 4;

const STATUS_EXPECTED = 1;
const STATUS_ARRIVED = 3;

@Component({
  selector: 'my-planner',
  templateUrl: './planner.component.html',
  styleUrls: [ './planner.component.css' ],
  providers: [
      // The locale would typically be provided on the root module of your application. We do it at
      // the component level here, due to limitations of our example generation script.
      {provide: MAT_DATE_LOCALE, useValue: 'nl-NL'},

      // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
      // `MatMomentDateModule` in your applications root module. We provide it at the component level
      // here, due to limitations of our example generation script.
      {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
      {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    ],
   
})

export class PlannerComponent implements OnInit, OnDestroy {
  startItem : Booking;
  hoverItem : Booking;
  hoverItemPrevious : Booking;

  targetItem: Booking;
  selectedBooking : Booking;

  isDown : boolean = false;
  isSetup : boolean = false;
  isLoading : boolean = false;

  roomGroups : RoomGroup[] = [];
  
  //rooms : RoomDetail[] = [];
  bookings = [];
  bookingDetails = [];  //PlannerDate
  bookingAttendees = [];
  //activities = [];
  times : Time[];

  showParticipants = false;
  isDebug = false;
  debugObject = 3;
  debugTime = '13:45';

  getColorCount = 0;
  isVisibleCount = 0;
  setColorsCount = 0;
  seconds = 900;

  futureOnly = false;

  private sub: any;
  private subRefresh: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private organizationService: OrganizationService,
    private plannerService: PlannerService,
    private paramService: ParamService,
    private dialogService: DialogService,
    private administrationService: AdministrationService, 
    private rlxDate: DateService,
    private batchService : BatchService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
    ) {
      //console.log('PlannerComponent.Create')

      this.showParticipants = this.paramService.getParam({moduleName: 'Planner', groupName: 'Inrichting', paramName: 'Deelnemers verbergen', paramValue : undefined}).paramValue == 'true';

      //this.dialogService.showMessage(window.devicePixelRatio.toString(), 'DPR')
    }

  accommodation() : Accommodation {
    return this.plannerService.selectedAccommodation;
  }


  getActivities() : Activity[] {
    var _result = [];

    if (this.hoverItem) {
      var _roomStart = Math.min(this.startItem ? this.startItem.room.number : this.hoverItem.room.number, this.hoverItem.room.number);
      var _roomEnd = Math.max(this.startItem ? this.startItem.room.number : this.hoverItem.room.number, this.hoverItem.room.number);

      var _groups = [];

      //loop alle objecten af en bepaal alle groepen waarin deze zich bevinden
      for (var _room = _roomStart; _room <= _roomEnd; _room++) {
        for (var rg = 0; rg < this.roomGroups.length; rg++) {
          for (var r = 0; r < this.roomGroups[rg].rooms.length; r++) {
            if (_room == this.roomGroups[rg].rooms[r].number) {
              if (_groups.indexOf(this.roomGroups[rg].name) == -1) {
                _groups.push(this.roomGroups[rg].name);
              }
            }
          }
        }
      }

      _result = this.organizationService.getActivitiesByObjectGroups(_groups);
    }

    return _result
  }


  toggleDebug() {
    this.isDebug = !this.isDebug;
  }

  showSnackBar(message : string) : void {
    this.snackBar.open(message, 'ok', {duration: 2500});
  }

  setSeconds(seconds) {
    this.plannerService.selectedAccommodation.timeLineInterval = seconds;
    this.getPlannerData(this.accommodation(), false)
  }

  getFontColor(time: Date, room : RoomDetail) : string {
    var _result = 'white';

    if (this.bookingDetails) {
      for (var b = 0; b < this.bookingDetails.length; b++) {
        if (room.number >= this.bookingDetails[b].roomStart && room.number <= this.bookingDetails[b].roomEnd) {
          if (this.rlxDate.timeToString(time) == this.rlxDate.timeToString(new Date(this.bookingDetails[b].timeStart))) {
            _result = this.bookingDetails[b].fontColor
          }
        }
      }
    }

    return _result;
  }

  getColor(time : Date, type : number, room : RoomDetail) : string {
    this.getColorCount++;

    var _booking = this.getBooking(time, room);

    if (_booking) {
      return _booking.backColor
    }

    if (this.rlxDate.isHistory(time)) {
      if (type == enum_planner_open) {
        if (!this.isFirstBookableItem(room, time) && this.isStartOfDay(time)) {
          return "gray";
        }
        else {
          return "lightgray";        
        }
      }
      else if (type == enum_planner_booking) {
        return "purple";
      }
      else if (type == enum_planner_closed) {
        return "black";
      }
      else {
        return "pink";
      }
    }
    else {
      if (room.member) {
        if (this.isEnum(enum_planner_booking, time, room)) {
          return "purple";
        }
        else if (this.isEnum(enum_planner_prime, time, room)) {
          return "mediumturquoise";
        }
        else if (this.isEnum(enum_planner_blockbooking, time, room)) {
          return "darkblue";
        }
        else if (this.isEnum(enum_planner_closed, time, room)) {
          return "black";
        }
        else if (this.isEnum(enum_planner_happyhour, time, room)) {
          return "orange";
        }
        else if (this.isEnum(enum_planner_open, time, room)) {
          return "paleturquoise";
        }
        else {
          return "red";
        }
      }
      else
      {
        if (this.isEnum(enum_planner_booking, time, room)) {
          return "cyan";
        }
        else if (this.isEnum(enum_planner_prime, time, room)) {
          return "DeepSkyBlue";
        }
        else if (this.isEnum(enum_planner_blockbooking, time, room)) {
          return "darkblue";
        }
        else if (this.isEnum(enum_planner_closed, time, room)) {
          return "black";
        }
        else if (this.isEnum(enum_planner_happyhour, time, room)) {
          return "orange";
        }
        else if (this.isEnum(enum_planner_open, time, room)) {
          return "lightskyblue";
        }
        else {
          return "maroon";
        }
      }
    }
  }

  isEnum(type : number,  time : Date, room : RoomDetail) : boolean {
    var _result = false;

    if (type == enum_planner_booking) {
      if (this.bookings) {
        for (var b = 0; b < this.bookings.length; b++) {
          if (room.number == this.bookings[b].room) {
            if (this.rlxDate.timeToString(time) == this.rlxDate.timeToString(new Date(this.bookings[b].date))) {
              if (this.bookings[b].quantity > 0) {
                _result = true;
              }
            }
          }
        }
      }
    }
    else {
      if (room.times) {
        for (var t = 0; t < room.times.length; t++) {
          if (room.times[t].enumIndex == type) {
            var startTime = new Date(room.times[t].startTime);
            var endTime = new Date(room.times[t].endTime);

            if (time >= startTime && time < endTime) {
              _result = true;
            }
          }
        }
      }
    }

    return _result;
  }

  getPlannerData(accommodation : Accommodation, refresh : boolean): void {
    this.isLoading = !refresh;

    this.plannerService.getRooms(this.administrationService.selectedAdministration, accommodation)
      .then(data => {
        if (data) {
          this.setPlannerData(data)
        }
        else {
          this.getPlannerData(accommodation, refresh)
        }
      } );
  }

  isFirstOfGroup(room : RoomDetail) : boolean {
    var _result = false;

    for (var rg = 0; rg < this.roomGroups.length; rg++) {
      if (this.roomGroups[rg].rooms.length) {
        if (room.number == this.roomGroups[rg].rooms[0].number ) {
          _result = true;
        }
      }
    }

    return _result;
  }

  isPartOfGroup(room, group : RoomGroup) {
    //console.log('isPartOfGroup - room = ' + room + ' - group = ' + JSON.stringify(group));

    var _result = false;

    for (var r = 0; r < group.rooms.length; r++) {
      if (group.rooms[r] == room) {
        _result = true
      }
    }

    return _result;
  }

  showTimeLine(group : RoomGroup) {
    var _result = false;

    if (group && this.roomGroups.length != 0) {
      _result = this.accommodation().showTimeLineShift &&
                group != this.roomGroups[0];              //voor de eerste groep hoeft het niet, dat wordt geregeld door showTimeLineLeft 


               //!(this.accommodation.showTimeLineLeft && group == this.roomGroups[0]) && //als je links aan hebt staan, dan wil je bij de eerste kamer niet nog een keer de tijdlijn
               //!(!this.accommodation.showTimeLineLeft && group == this.roomGroups[0]);  //als je links uit hebt staan, dan ook niet bij de eerste kamer tonen
    }

    return _result;
  }

  isStart(room: RoomDetail, time : Date) : boolean {
    var _log = (this.isDebug && (room.number == this.debugObject) && this.rlxDate.timeToString(time) == this.debugTime);

    //if (_log) { console.log('time = ' + time + ' - roomtimes: ' + JSON.stringify(room.times)) }

    var _start = this.rlxDate.dateOnly(this.rlxDate.addDays(this.plannerService.selectedDate, 1)); //end of selected day

    var _type = 0;

    for (var t = 0; t < room.times.length; t++) {
      //if (room.times[t].enumIndex == enum_planner_open) {  //we moeten in ieder type tijdsvak kijken
        if (new Date(room.times[t].startTime) <= time && new Date(room.times[t].endTime) > time && room.times[t].enumIndex > _type) {
          _type = room.times[t].enumIndex;
          _start = new Date(room.times[t].startTime);
        }
      //}
    }

    //if (_log) { console.log('START - ' + _start)}

    var _seconds = 0;

    for (var t = 0; t < room.times.length; t++) {
      
        var startTime = new Date(room.times[t].startTime);
        var endTime = new Date(room.times[t].endTime);

        if ((time >= startTime) && (time < endTime)) {

          var _interval = new Date(room.times[t].bookingInterval) ;
          var _value = new Date('1970-01-01 ' + _interval.getUTCHours() + ':' + _interval.getUTCMinutes());

          _seconds = this.rlxDate.toSeconds(_value);

          //if (_log) {
            //console.log('interval = ' + _value + '  - seconds --> ' + _seconds);
          //}
        }
    }

    if (_seconds == 0) {
      _seconds = this.accommodation().bookingInterval;
    }

    while (time > _start) {
      time = this.rlxDate.addSeconds(time, - _seconds)
    }

    return this.rlxDate.isSameTime(time, _start);
  }


  getInterval(room : RoomDetail, time : Date) : number {
    var _seconds = this.accommodation().bookingInterval;

    //de sortering van de TimeIntervals is op basis van de enumIndex, daardoor komen de openingstijden als eerste langs,
    //als er vervolgens een interval van een ander type langskomt, waar dit tijdstip in valt, dan zal die daarna de openingstijd overrulen
    //je wilt dus het laatste TimeInterval hebben dat voldoet
    for (var t = 0; t < room.times.length; t++) {
      var startTime = new Date(room.times[t].startTime);
      var endTime = new Date(room.times[t].endTime);


      if (time >= startTime && time < endTime) {
        //console.log('time = ' + time + '  -  start = ' + startTime + '  -  end = ' + endTime);

        var _interval = new Date(room.times[t].bookingInterval);

        _seconds = this.rlxDate.toSeconds(new Date('1970-01-01 ' + _interval.getUTCHours() + ':' + _interval.getUTCMinutes()));

        if (_seconds == 0)  {
          _seconds = this.accommodation().bookingInterval;
        }
      }
    }

    return _seconds;
  }

  getType(room : RoomDetail, time : Date) : number {
    var _type = enum_planner_open
    var _log = (this.isDebug && (room.number == this.debugObject) && this.rlxDate.timeToString(time) == this.debugTime);

    if (_log) { 'time : ' + time + ' - room : ' + console.log(JSON.stringify(room)); }

    //de sortering van de TimeIntervals is op basis van de enumIndex, daardoor komen de openingstijden als eerste langs,
    //als er vervolgens een interval van een ander type langskomt, waar dit tijdstip in valt, dan zal die daarna de openingstijd overrulen
    //je wilt dus het laatste TimeInterval hebben dat voldoet
    for (var t = 0; t < room.times.length; t++) {
      var _time = new Date(  this.rlxDate.dateToJSString(this.plannerService.selectedDate) + ' ' + this.rlxDate.timeToString(time));

      if ((_time >= new Date(room.times[t].startTime)) && (_time <= new Date(room.times[t].endTime))) {
        _type = room.times[t].enumIndex;
      }
    }

    if (_log) { console.log('getType = ' + _type); }

    if (this.bookings) {
      for (var b = 0; b < this.bookings.length; b++) {
        if (room.number == this.bookings[b].room) {
          if (this.rlxDate.timeToString(time) == this.rlxDate.timeToString(new Date(this.bookings[b].date))) {
            if (this.bookings[b].quantity > 0) {
              //console.log('ja hoor: room = ' + this.bookings[b].room + ' - time = ' + this.bookings[b].date + ' - quantity = ' + this.bookings[b].quantity);

              _type = enum_planner_booking;
            }
          }
        }
      }
    }

    return _type;
  }

  bookingCaption(room : RoomDetail, time : Date) : string {
    var _caption = '';

    if (this.bookingDetails) {
      for (var b = 0; b < this.bookingDetails.length; b++) {
        if (room.number >= this.bookingDetails[b].roomStart && room.number <= this.bookingDetails[b].roomEnd) {
          if (this.rlxDate.timeToString(time) == this.rlxDate.timeToString(new Date(this.bookingDetails[b].timeStart))) {

            if (this.plannerService.user.hasRight('ShowReservations') || this.bookingDetails[b].Booker == this.plannerService.user.code) {
              _caption = this.rlxDate.timeToString(time) + '\r\n' + this.bookingDetails[b].NameBooker;
            }
            else {
              _caption =  this.rlxDate.timeToString(time) + '\r\n' + 'bezet';
            }
          }
        }
      }
    }

    return _caption;
  }

  bookingReservation(room : RoomDetail, time : Date) : string {
    var _value = '';

    var _booking = this.getBooking(time, room);

    if (_booking) {
      _value = _booking.reservation;
    }

    return _value;
  }

  bookingReservationDetailNumber(room : RoomDetail, time : Date) : string {
    var _value = '';

    var _booking = this.getBooking(time, room);

    if (_booking) {
      _value = _booking.reservationDetailNumber;
    }

    return _value;
  }

  bookingType(room : RoomDetail, time : Date) : string {
    var _value = '';

    if (this.bookingDetails) {
      for (var b = 0; b < this.bookingDetails.length; b++) {
        if (room.number >= this.bookingDetails[b].roomStart && room.number <= this.bookingDetails[b].roomEnd) {
          if (this.rlxDate.timeToString(time) == this.rlxDate.timeToString(new Date(this.bookingDetails[b].timeStart))) {
            _value = this.bookingDetails[b].reservationType
          }
        }
      }
    }

    return _value;
  }

  futureOnlyChange(event) {
    this.futureOnly = (event.checked);
    this.refreshPlanner();
  }

  bookingRelation(room : RoomDetail, time : Date) : number {
    var _value = 0;
  
    if (this.bookingDetails) {
      for (var b = 0; b < this.bookingDetails.length; b++) {
        if (room.number >= this.bookingDetails[b].roomStart && room.number <= this.bookingDetails[b].roomEnd) {
          if (this.rlxDate.timeToString(time) == this.rlxDate.timeToString(new Date(this.bookingDetails[b].timeStart))) {
            _value = this.bookingDetails[b].relation
          }
        }
      }
    }

    return _value;
  }

  getBooking(time, room) : any {
    var _result = undefined;

    if (this.bookingDetails) {
      for (var b = 0; b < this.bookingDetails.length; b++) {
        var _booking = this.bookingDetails[b];

        if (room.number >= _booking.roomStart && room.number <= _booking.roomEnd) {
          if (time >= new Date(_booking.timeStart) &&
              time <  new Date(_booking.timeEnd)) {
            _result = _booking;
          }
        }
      }
    }

    return _result;
  }

  isVisible(time, room, group) : boolean {
    this.isVisibleCount++;

    var _result = false;

    var _log = (this.isDebug && room.number == this.debugObject && this.rlxDate.timeToString(time) == this.debugTime);

    if (!this.isTimeVisible(time)) {
      return _result;
    }

    if (this.isPartOfGroup(room, group)) {
      var _booking = this.getBooking(time, room);

      if (this.isStartOfDay(time)) {
        _result = true;

        if (this.futureOnly) {
          if (_booking && (!this.isFirstOfGroup(room) && _booking.roomStart != room.number)) {
            _result = false;
          }
        }

        if (_log) { console.log('--> isStartOfDay') }
      }
      else
      if (_booking) {
        if (this.rlxDate.isSameTime(time, new Date(_booking.timeStart)) && (this.isFirstOfGroup(room) || _booking.roomStart == room.number) ) { // this.isStartOfBooking(room, time))
          _result = true;

          if (_log) { console.log('--> isStartOfBooking') }
        }
      }
      else
      if (_booking) { //this.isPartOfBooking(room, time))
        _result = false;

        if (_log) { console.log('--> isPartOfBooking') }
      }
      else
      if (this.isStart(room, time)) {
        _result = true;

        if (_log) { console.log('--> isStart') }
      }
      else
      if (this.isFirstBookableItem(room, time)) {
        _result = true;

        if (_log) { console.log('--> isFirstBookableItem') }
      }
      else {
        if (_log) { console.log('--> helaas...NIET ZICHTBAAR') }
      }
    }

    //console.log(_result + ' - ' + JSON.stringify(time) + ' - ' + room.number + ' - ' + group.name);

    return _result;
  }

  isStartOfDay(time) {
    var _log = (this.isDebug && this.rlxDate.timeToString(time) == this.debugTime);

    var _start = this.rlxDate.dateOnly(this.rlxDate.addDays(this.plannerService.selectedDate, 1));

    for (var rg= 0; rg < this.roomGroups.length; rg++) {
      for (var t = 0; t < this.roomGroups[rg].rooms[0].times.length; t++) {
        if (this.roomGroups[rg].rooms[0].times[t].enumIndex == enum_planner_open) {
          //if (_log) { console.log('TIJDSTIP - ' + this.roomGroups[rg].rooms[0].times[t].startTime) }

          if (new Date(this.roomGroups[rg].rooms[0].times[t].startTime) < _start) {
            _start = new Date(this.roomGroups[rg].rooms[0].times[t].startTime);
          }
        }
      }
    }

    //if (_log) { console.log('time = ' + time + ' - start = ' + _start) }

    if (this.futureOnly && this.rlxDate.isToday(this.plannerService.selectedDate)) {
      var _huidig = this.rlxDate.addHours(new Date(), - 1);

      while (_huidig > _start) {
        _start = this.rlxDate.addSeconds(_start, this.accommodation().timeLineInterval);
      }
    }

    return (this.rlxDate.timeToString(_start) == this.rlxDate.timeToString(time));
  }

  isFirstBookableItem(room, time) : boolean {
    var _result = true;
    var _time = time;
    var _log = (this.isDebug && room.number == this.debugObject && this.rlxDate.timeToString(time) == this.debugTime);

    /*
    ga op zoek naar het begin van dit item en kijk vervolgens of er boekingen zijn op dit tijdvak
    als er geen boekingen zijn op dit tijdvak en het begin van het tijdvak wijkt af van de gecheckte tijd,
    dan is de gecheckte tijd niet het boekbare begin van dit tijdvak.
    */

    //console.log('isFirstBookableItem');

    var _today = this.rlxDate.today();

    while (!this.isStart(room, _time) && _time > _today) {
      _time = this.rlxDate.addSeconds(_time, - this.accommodation().timeLineInterval);
    }

    if (_log) { console.log('TIME = ' + _time) }

    if (!this.isPartOfBooking(room, _time)) {
      if (_time != time) {
        _result = false;
      }
    }


    if (_result) {
      if (!this.isStart(room, time)) {
        //ga terug naar het begin van de boeking en tel het aantal lege vakjes wat je tegenkomt, als dat 0 is, dan zou dit het begin van een gebroken tijdvak moeten zijn

        _time = time;

        var _einde = false;
        var _free_items = 0;


        while (!_einde) {
          _time = this.rlxDate.addSeconds(_time, - this.accommodation().timeLineInterval);

          if (!this.isPartOfBooking(room, _time)) {
            _free_items++;
          }

          if (this.isStart(room, _time)) {
            _einde = true;
          }
        }

        //if (_log) { console.log('PREVIOUS = ' + this.rlxDate.timeToString(_time) + ' --> ' + _previous_free) }

        _result = (_free_items == 0);
      }
    }

    return _result;
  }

  isTimeVisible(time) : boolean {
    var _result = true;

    //console.log(this.rlxDate.dateOnly(new Date()) + ' - ' + this.rlxDate.dateOnly(this.plannerService.selectedDate));

    if (this.futureOnly && this.rlxDate.isToday(this.plannerService.selectedDate)) {
      if (new Date(time).getTime() < new Date(new Date().getTime() - 60*60*1000).getTime()) {
        _result = false;
      }
    }

    return _result;
  }


  setPlannerData(data) : void {
    //console.log('setPlannerData - START');

    var _times = this.batchService.getRecordSet(data, ['enumIndex', 'startTime', 'endTime', 'timeLineinterval', 'bookingInterval', 'number', 'name', 'member']);

    this.bookings = this.batchService.getRecordSet(data, ['room', 'date', 'quantity']);
    this.bookingDetails = this.batchService.getRecordSet(data, ['timeStart', 'timeEnd', 'roomStart', 'roomEnd', 'Booker', 'reservation', 'NameBooker', 'fontColor', 'backColor', 'reservationType', 'reservationDetailNumber', 'status', 'statusCode']);
    this.bookingAttendees = this.batchService.getRecordSet(data, ['reservationNumber', 'reservationDetailNumber', 'name', 'code', 'Status'])

    var roomsPerGroup = this.batchService.getRecordSet(data, ['number', 'name', 'groupName', 'member']);

    this.roomGroups = [];

    if (roomsPerGroup) {
      //een groep kamers is uniek op basis van de naam
      for (var r = 0; r < roomsPerGroup.length; r++) {
        var _gevonden = false;

        for (var rg = 0; rg < this.roomGroups.length; rg++) {
          if (this.roomGroups[rg].name == roomsPerGroup[r].groupName) {
            _gevonden = true;
          }
        }

        if (!_gevonden) {
        //deze groep bestaat nog niet, voeg hem toe zonder kamers
          this.roomGroups.push({name: roomsPerGroup[r].groupName, rooms: []});
        }


        //als het goed is bestaat inmiddels de groep in het buffer, zorg ervoor dat de room toegevoegd wordt aan de groep
        for (var rg = 0; rg < this.roomGroups.length; rg++) {
          if (this.roomGroups[rg].name == roomsPerGroup[r].groupName) {

            this.roomGroups[rg].rooms.push({name: roomsPerGroup[r].name, member: roomsPerGroup[r].member, number: roomsPerGroup[r].number, times: []}); 
          }
        }
      }
    }

    //console.log(JSON.stringify(this.roomGroups));

    //de groepen zijn gevuld, met daarbinnen de kamers, koppel nu de tijden eraan
    for (var t = 0; t < _times.length; t++) {
      for (var rg = 0; rg < this.roomGroups.length; rg++) {
        for (var r = 0; r < this.roomGroups[rg].rooms.length; r++) {
          if (_times[t].number == this.roomGroups[rg].rooms[r].number) {
            this.roomGroups[rg].rooms[r].times.push({startTime: _times[t].startTime, endTime: _times[t].endTime, bookingInterval: _times[t].bookingInterval, timeLineInterval: _times[t].timeLineInterval, enumIndex: _times[t].enumIndex});
          }
        }
      }
    }

    //console.log(JSON.stringify(this.roomGroups));

    var _now : Date = new Date();
    var start : Date;
    var end : Date;

    //console.log(JSON.stringify(this.rooms));

    //bepaal de laagste starttijd en hoogste eindtijd
    for (var rg = 0; rg < this.roomGroups.length; rg++) {
      for (var r = 0; r < this.roomGroups[rg].rooms.length; r++) {
        for (var t = 0; t < this.roomGroups[rg].rooms[r].times.length; t++) {
          if (this.roomGroups[rg].rooms[r].times[t].enumIndex == enum_planner_open) {
            var _temp : Date = new Date(this.roomGroups[rg].rooms[r].times[t].startTime);

            //console.log('this.rooms[r].times[t].startTime = ' + typeof this.rooms[r].times[t].startTime);
            //console.log('_temp = ' + typeof _temp);


            //console.log('start = ' + start + ' - temp = ' + _temp);

            if ((start == undefined) || (start > _temp)) {
              start = _temp
            }

            _temp = new Date(this.roomGroups[rg].rooms[r].times[t].endTime);

            if (_temp < start) {
              _temp = new Date(_temp.getTime() + 24*60*60*1000);
            }

            //console.log('end = ' + end + ' - temp = ' + _temp);

            if ((end == undefined) || (end < _temp)) {
              end = _temp
            }
          }
        }
      }
    }

    if (start > end) {
      end = new Date(end.getTime() + 24*60*60*1000);
    }

    //console.log('start = ' + start + ' - end = ' + end);
    //console.log('seconden = ' + this.accommodation.timeLineInterval);

    this.times = [];

    if (this.accommodation() ) {
      while (start < end) {

        if (this.isTimeVisible(start)) {
          this.times.push({items:[], time: start, isStart: false});
        }

        //console.log('TYPE : ' + typeof start);

        start = new Date(start.getTime() + this.accommodation().timeLineInterval * 1000);
      }
    }

    //console.log(JSON.stringify(this.times));

    //console.log('TIMES = ' + this.times.length);
    //console.log('GROUPS = ' + this.roomGroups.length);
    var _color = "white";
    var _fontColor = "white"

    for (var t = 0; t < this.times.length; t++) {

      for (var rg = 0; rg < this.roomGroups.length; rg++) {
        //console.log('ROOMS = ' + this.roomGroups[rg].rooms.length);

        var _roomGroup = this.roomGroups[rg];

        for (var r = 0; r < _roomGroup.rooms.length; r++) {

          var _time = this.times[t].time;
          var _room = _roomGroup.rooms[r];

          if (this.isVisible(_time, _room, _roomGroup)) {

            var _status = 1;
            var _statusCode = '';
            var _rowSpan = this.getRowSpan(_room, _time, false);
            var _now = new Date();
            var _reservation = this.bookingReservation(_room, _time);
            var _reservationDetailNumber = this.bookingReservationDetailNumber(_room, _time);
            var _type = this.bookingType(_room, _time);
            var _relations = this.getRelations(_reservation);
            var _participants = [];
            var _timeEnd = new Date(_time.getTime() + (_rowSpan * this.accommodation().timeLineInterval * 1000));
            var _defaultRowSpan = this.getRowSpan(_room, _time, true);
            var _timeString = this.rlxDate.timeToString(_time);

            var _booking = this.getBooking(_time, _room);

            if (_booking) {
              _timeString = this.rlxDate.timeToString(new Date(_booking.timeStart));
              _statusCode = _booking.statusCode;

              if (_booking.status == 'Binnen') {
                _status = STATUS_ARRIVED
              } 
            }

            if (_defaultRowSpan != _rowSpan) {
              _timeString = _timeString + ' - ' + this.rlxDate.timeToString(_timeEnd);
            }

            //if (_type == "group") {
               _participants = this.getParticipants(_reservation, _reservationDetailNumber);
            //}

            _color = this.getColor(_time, this.getType(_room, _time), _room);
            _fontColor = this.getFontColor(_time, _room);


            this.times[t].items.push({relation: this.bookingRelation(_room, _time),
                                      reservation: _reservation,
                                      reservationDetailNumber: _reservationDetailNumber,
                                      caption: this.bookingCaption(_room, _time),
                                      type: this.getType(_room, _time),
                                      room: _room,
                                      timeStart: _time,
                                      timeEnd:_timeEnd ,
                                      color: _color,
                                      fontColor: _fontColor,
                                      colorOriginal: _color,
                                      rowSpan: _rowSpan,
                                      colSpan: this.getColSpan(_room, _time),
                                      isPartial: _rowSpan < _defaultRowSpan,
                                      isSelected: false,
                                      isHistory: (this.futureOnly || _time < _now),
                                      timeString:  _timeString,
                                      relations: _relations,
                                      participants: _participants,
                                      isGroup: (_type == "group"),
                                      isSelectedBooking: false,
                                      showTime: _reservation ? true : false,
                                      status: _status,
                                      statusCode: _statusCode
                                    })
          }
        }
      }
    }

    this.isLoading = false;

    this.setColors();
  }

  getRelations(reservation) : Relation[] {
    var _result = [];

    if (this.bookingDetails) {
      for (var b = 0; b < this.bookingDetails.length; b++) {
        if (this.bookingDetails[b].reservation == reservation) {

          if (this.bookingDetails[b].Booker) {
            if (this.bookingDetails[b].Booker == this.plannerService.user.code || this.plannerService.user.hasRight('ShowReservations') || this.bookingDetails[b].reservationType == "group") {
              _result.push({code: this.bookingDetails[b].Booker, name: this.bookingDetails[b].NameBooker, isMale: undefined})
            }
          }

          if (this.plannerService.user.hasRight('ShowReservations')) {
            if (this.bookingDetails[b].Attendee) {
              _result.push({code: this.bookingDetails[b].Attendee, name: this.bookingDetails[b].NameAttendee, isMale: undefined})
            }
          }
        }
      }
    }

    return _result;
  }

  selectRelation(event, code) {
    //console.log('selectRelation(' + code + ')');

    event.stopPropagation();
  }

  getParticipants(reservation, detail) : Relation[] {
    var _result = [];

    if (this.bookingAttendees) {
      for (var i = 0; i < this.bookingAttendees.length; i++) {
        if (this.bookingAttendees[i].reservationNumber == reservation &&
            this.bookingAttendees[i].reservationDetailNumber == detail) {
          _result.push({code: this.bookingAttendees[i].code, name: this.bookingAttendees[i].name, isMale: true});
        }
      }
    }

    return _result;
  }

  getRowSpan(room : RoomDetail, time : Date, default_span : boolean) : number {
    /*
    Loop vanaf dit tijdstip met stapjes ter grootte van de tijdlijn naar het begin van het volgende vakje
    Tel het aantal tijdvakken dat je dan tegenkomt
    */
    var _log = (this.isDebug && (room.number == this.debugObject) && (this.rlxDate.timeToString(time) == this.debugTime));
    var _span : number = 1;



    var _time = new Date(time);

    if (default_span) {
      while (!this.isStart(room, _time) && !this.isStartOfDay(_time)) {
        _time = new Date(_time.getTime() - this.accommodation().timeLineInterval * 1000);
      }
    }

    _time =  new Date(_time.getTime() + this.accommodation().timeLineInterval * 1000);

    while (!this.isStart(room, _time)) {
      _time = new Date(_time.getTime() + this.accommodation().timeLineInterval * 1000);
      _span++
    }

    if (_log) { console.log('getRowSpan FASE 1 - ' + _span)}

    if (this.bookingDetails) {
      if (!default_span) {
      /*
      Loop van het einde van het tijdvak weer terug naar het begin en als je een boeking op dat specifieke tijdstip vindt, dan verklein je de span
      */
        while (_time > time) {
          for (var b = 0; b < this.bookingDetails.length; b++) {
            if (room.number >= this.bookingDetails[b].roomStart && room.number <= this.bookingDetails[b].roomEnd) {
              if (new Date(this.bookingDetails[b].timeStart).getTime() < _time.getTime() && new Date(this.bookingDetails[b].timeEnd).getTime() > _time.getTime()) {

                if (_log) { console.log('getRowSpan FASE 1a - ' + _time)}

                _span--;
              }
            }
          }

          _time = new Date(_time.getTime() - this.accommodation().timeLineInterval * 1000);
        }
      
      
        if (_log) { console.log('getRowSpan FASE 2 - ' + _span)}

        /*
        Kijk vervolgens of er op dit moment een boeking gemaakt is
        --------------------------------------------------------------------------------
        Een boeking kan over meerdere tijdvakken gaan, of een gedeelte ervan
        Daardoor is de hierboven bepaalde standaard duur niet meer van belang
        --------------------------------------------------------------------------------
        */

        for (var b = 0; b < this.bookingDetails.length; b++) {
          if (room.number >= this.bookingDetails[b].roomStart && room.number <= this.bookingDetails[b].roomEnd) {
            if (new Date(this.bookingDetails[b].timeStart).getTime() <= time.getTime() && new Date(this.bookingDetails[b].timeEnd).getTime() > time.getTime()) {
              _span = (new Date(this.bookingDetails[b].timeEnd).getTime() - new Date(this.bookingDetails[b].timeStart).getTime()) / (this.accommodation().timeLineInterval * 1000);
            }
          }
        }

        if (_log) { console.log('getRowSpan FASE 3 - ' + _span)}

        /*
        Voorbeeld Squash
        --------------------------------------------------------------------------------
        Baan 1 opent om 10:00
        Baan 6 opent om 10:15.

        Baan 1 heeft om 10:00 een tijdvak van 45 minuten om op te boeken
        Baan 6 heeft om 10:15 een tijdvak van 45 minuten om op te boeken
        Baan 6 heeft om 10:00 een tijdvak van 15 minuten waarop NIET geboekt kan worden
        --------------------------------------------------------------------------------
        */
        var _found = false;

        for (var t = 0; t < room.times.length; t++) {
          if (!_found && room.times[t].enumIndex == enum_planner_open) {
            _found = true;

            var _start = new Date (room.times[t].startTime);

            if (time < _start) {
              _time = time;
              _span = 0;


              while (_time < _start) {
                _time = new Date(_time.getTime() + this.accommodation().timeLineInterval * 1000)
                _span++;
              }
            }
          }
        }

        if (_log) { console.log('getRowSpan FASE 4 - ' + _span)}

        if (this.futureOnly) {
          var _booking = this.getBooking(time, room);

          if (_booking) {
            var _time = new Date(_booking.timeStart);
            var _timeEnd = new Date(_booking.timeEnd);
            var _count = false;

            while (this.rlxDate.addSeconds(_time, this.accommodation().timeLineInterval) < _timeEnd) {
              _time = this.rlxDate.addSeconds(_time, this.accommodation().timeLineInterval);

              if (this.isStartOfDay(_time) ) {
                _span = 0;
                _count = true;
              }

              if (_count) { _span++ }
            }
          
          }

        }

      }
    }

    return _span;
  }

  getColSpan(room : RoomDetail, time : Date) : number {
    var _log = (this.isDebug && (room.number == this.debugObject) && (this.rlxDate.timeToString(time) == this.debugTime));

    var _result = 1;

    //bepaal de roomgroup waar deze room in valt
    var _rg = -1;

    for (var rg = 0; rg < this.roomGroups.length; rg++) {
      for (var r = 0; r < this.roomGroups[rg].rooms.length; r++) {
        if (this.roomGroups[rg].rooms[r] == room) {
          _rg = rg;
        }
      }
    }


    //bepaal vervolgens hoeveel rooms uit de boeking binnen de roomgroup vallen
    //stel ik maar een boeking van baan 3 tot 8, dan vallen 3,4,5 in een andere groep dan 6,7,8
    //als je tussen de groepen een tijdlijn wilt weer kunnen geven, dan is het niet mogelijk om de colspan 6 te maken,
    //maar heb je 2 items van 3 banen nodig
    if (_rg != -1) {
      _result = 0;

      if (this.bookingDetails) {
        for (var b = 0; b < this.bookingDetails.length; b++) {
          if (this.bookingDetails[b].roomStart <= room.number && this.bookingDetails[b].roomEnd >= room.number) {
            if (new Date(this.bookingDetails[b].timeStart).getTime() == time.getTime() ||
                this.futureOnly && new Date(this.bookingDetails[b].timeStart).getTime() <= time.getTime() && this.isStartOfDay(time)) {

              for (var r = 0; r < this.roomGroups[_rg].rooms.length; r++) {
                if (this.roomGroups[_rg].rooms[r].number >= this.bookingDetails[b].roomStart &&  this.roomGroups[_rg].rooms[r].number <= this.bookingDetails[b].roomEnd) {
                  _result++;
                }
              }
            }
          }
        }
      }

      if (_log) { console.log('COLSPAN = ' + _result + ' - roomGroup = ' + _rg) }
    }


    return _result;
  }

  isStartOfBooking(room : RoomDetail, time : Date) : boolean {
    var _result = false;

    //situatie:
    //2 groepen met objecten, baan 1/5 en baan 6/11, tijdlijn in het midden
    //boeking op baan 3 t/m 8 van 19:00 tot 20:00
    //
    //baan 3 om 19:00 is de start van de boeking, dat is duidelijk
    //maar doordat de helft in een andere objectGroup valt, is baan 6 om 19:00 eigenlijk ook de start van de boeking
    //in de planner moeten immers 2 losse items getoond worden van 3 banen breed
    if (this.bookingDetails) {
      for (var b = 0; b < this.bookingDetails.length; b++) {
        if (new Date(this.bookingDetails[b].timeStart).getTime() == time.getTime() || (this.futureOnly && this.isStartOfDay(time)) ) {
          if (room.number == this.bookingDetails[b].roomStart) {
            _result = true
          }
          else {
            if (room.number >= this.bookingDetails[b].roomStart && room.number <= this.bookingDetails[b].roomEnd) {
              for (var rg = 0; rg < this.roomGroups.length; rg++) {
                if (this.roomGroups[rg].rooms && this.roomGroups[rg].rooms[0].number == room.number) {            //is het object de eerste van een groep? zo ja, dan moet hier ook een boeking getoond worden! (baan 6)  
                  _result = true;
                }
              }
            }
          }
        }
      }
    }

    return _result;
  }

  isPartOfBooking(room : RoomDetail, time : Date) : boolean {
    var _result : boolean = false;

    if (this.bookingDetails) {
      for (var b = 0; b < this.bookingDetails.length; b++) {
        if (this.bookingDetails[b].roomStart <= room.number && this.bookingDetails[b].roomEnd >= room.number) {
          //console.log('booking = ' + JSON.stringify(this.bookingDetails[b]));
          //console.log('time = ' + time);

          if (new Date(this.bookingDetails[b].timeStart) <= time && new Date(this.bookingDetails[b].timeEnd) > time) {
            _result = true;
          }
        }
      }
    }

    return _result;
  }

  isHour(time : Date): boolean {
    return time.getMinutes() == 0; 
  }

  getCaption(time : Date) : string {
    if (this.isHour(time)) {
      return this.rlxDate.timeToString(time);
    }
    else {
      return '' + time.getMinutes();
    }
  }

  selectPlanner(params, refresh : boolean) : void {
     this.plannerService.selectedAccommodation = this.plannerService.accommodationByCode(params['id']);

     //console.log(JSON.stringify(this.accommodation))

     this.getPlannerData(this.accommodation(), refresh);
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {this.selectPlanner(params, false)})

    this.startAutoRefresh();
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.stopAutoRefresh();
  }

  startAutoRefresh() {
    this.subRefresh = 
      IntervalObservable.create(30000)
        .subscribe(() => {
          //console.log('AUTO-REFRESH');

          if (this.plannerService.mode === 'browse' && !this.isDown && !this.plannerService.timedOut) {
            this.selectPlanner({id: this.accommodation().nameShort}, true);
          }
        })
  }

  stopAutoRefresh() {
    this.subRefresh.unsubscribe()
  }

  hasReservation(start, end) : boolean {
    var _result = false;

    if (start && end) {
      //console.log(start.timeStart + '-' + start.timeEnd + '  -  ' + end.timeStart + '-' + end.timeEnd);

      var _roomStart = Math.min(start.room.number, end.room.number);
      var _roomEnd = Math.max(start.room.number, end.room.number);

      var _timeStart = new Date(Math.min(new Date(start.timeStart).getTime(), new Date(end.timeStart).getTime()));
      var _timeEnd = new Date(Math.max(new Date(start.timeEnd).getTime(), new Date(end.timeEnd).getTime()));

      //console.log("object " + _roomStart + '-' + _roomEnd + " - time " + _timeStart + '-' + _timeEnd);

      for (var rg = 0; rg < this.roomGroups.length; rg++) {
        for (var r = 0; r < this.roomGroups[rg].rooms.length; r++) {
          var _room = this.roomGroups[rg].rooms[r];

          if (_roomStart <= _room.number && _roomEnd >= _room.number) {
            var _time = new Date(_timeStart);

            while (!_result && _time < _timeEnd) {
              if (this.isPartOfBooking(_room, _time)) {
                _result = true;
              }

              _time = new Date(_time.getTime() + this.accommodation().timeLineInterval * 1000);
            }
          }
        }
      }
    }

    return _result;
  }


  rightClick(item): void {
    if (!this.targetItem && this.isValidTarget(item)) {
      this.targetItem = item;
    }

    if (this.targetItem) {
      this.startMove(this.targetItem);
      return
    }

    //console.log('ITEM = ' + JSON.stringify(item))

    var _log = (this.isDebug && (item.room.number == this.debugObject) && this.rlxDate.timeToString(item.timeStart) == this.debugTime);

    if (_log) { console.log('rightClick - item - ' + JSON.stringify(item)); }

    //if (item.type != enum_planner_booking && this.selectedBooking && (this.plannerService.mode == 'move' || this.plannerService.mode == 'copy')) {
    ////verplaatsen van een boeking naar een leeg tijdstip...
    //  console.log('SCENARIO 1');

    //  if (this.allowMove(item) ) {
    //    this.startMove(item);
    //  }
    //}
    //else
    if (this.rlxDate.isHistory(item.timeStart)) {
      this.dialogService.showMessage('Deze boeking is al voorbij', 'Sportplanner')
    }
    else if (!item.relation &&
             !this.rlxDate.isHistory(item.timeStart) &&
             item.type != enum_planner_closed &&
             item.type != enum_planner_blockbooking &&
             (item.type != enum_planner_booking || item.isGroup)) {
      //console.log('SCENARIO 2');

      if (!this.isDebug && this.hasReservation(this.startItem, this.hoverItem)) {
        //console.log('ERROR 1')

        var _tekst = 'Uw selectie bevat al reservingen\n\nHet is niet mogelijk om hier een boeking te maken\n\nMaak een andere selectie';

        if (this.isDebug) {
          _tekst = 'DEBUG MODE - ' + _tekst;
        }

        this.dialogService.showMessage(_tekst, 'Sportplanner');
      }
      else
      if (!this.isDebug && this.selectionHasItemsOfType(enum_planner_closed)) {
        var _tekst = 'U kunt hier niet boeken\n\nEen gedeelte van uw selectie is gesloten';

        if (this.isDebug) {
          _tekst = 'DEBUG MODE - ' + _tekst;
        }

        this.dialogService.showMessage(_tekst, 'Sportplanner');
      }
      else
      if (!this.isDebug && this.selectionHasItemsOfType(enum_planner_blockbooking)) {
        var _tekst = 'U kunt hier niet boeken\n\nEen gedeelte van uw selectie is geblokkeerd';

        if (this.isDebug) {
          _tekst = 'DEBUG MODE - ' + _tekst;
        }

        this.dialogService.showMessage(_tekst, 'Sportplanner');
      }
      else {
        this.stopAutoRefresh();

        let dialogRef = this.dialog.open(DialogToevoegen, { panelClass: 'dialog', width: '640px', data: { item : item, start : this.startItem || this.hoverItem, end : this.hoverItem, accommodation : this.accommodation(), activities : this.getActivities(), isParticipant : this.isParticipant(item, this.plannerService.user.code) }});

        if (window.innerWidth < 720) {
          dialogRef.updatePosition({ top: '10px' } )
        }

        dialogRef.afterClosed().subscribe(result => {
          this.startItem = undefined;
          this.hoverItem = undefined;
          this.hoverItemPrevious = undefined;

          this.getPlannerData(this.accommodation(), true);

          this.startAutoRefresh();
        });
      }
    }
    else if (this.selectedBooking == item && item.type == enum_planner_booking) {
      //console.log('SCENARIO 2b');

      var _options = [];

      if (this.allowMove(item)) {
        _options.push({index: OPTION_MOVE, caption: 'verplaatsen'})
      }

      if (this.allowCopy(item)) {
        _options.push({index: OPTION_COPY, caption: 'herhalen'})
      }

      if (this.allowDelete(item)) {
        _options.push({index: OPTION_DELETE, caption: 'verwijderen'})
      }

      _options.push({index: OPTION_ARRIVE, caption: 'aanwezig'})

      if (_options.length) {
        this.stopAutoRefresh();

        var _dialog = this.dialogService.optionBox('Wat wilt u met deze boeking doen?\n\n' + this.bookingToHTML(this.selectedBooking), 'Sportplanner', _options, this, undefined);

        _dialog.afterClosed().subscribe(result => {
          this.startAutoRefresh();

          if (result.index == OPTION_MOVE) {
            var _dialog = this.rlxDate.pickDate(new Date());

            _dialog.afterClosed().subscribe(result => {
              if (result && result.date) {
                if (!this.rlxDate.isSameTime(this.rlxDate.dateOnly(this.plannerService.selectedDate), this.rlxDate.dateOnly(result.date))) {
                  this.plannerService.selectedDate = result.date
                  this.refreshPlanner();
                }

                this.plannerService.mode = "move";
              }
            })
          }

          if (result.index == OPTION_COPY) {
            this.plannerService.mode = "copy";

            var _dialog = this.rlxDate.pickDate(new Date());

            _dialog.afterClosed().subscribe(result => {
              if (result && result.date) {
                if (!this.rlxDate.isSameTime(this.rlxDate.dateOnly(this.plannerService.selectedDate), this.rlxDate.dateOnly(result.date))) {
                  this.plannerService.selectedDate = result.date
                  this.refreshPlanner();
                }
              }
            })
          }

          if (result.index == OPTION_DELETE) {
            this.stopAutoRefresh();

            var _dialog = this.dialogService.confirm('Weet u zeker dat u de boeking wilt verwijderen?', 'Sportplanner');

            _dialog.afterClosed().subscribe(result => {
              this.startAutoRefresh();

              if (result.caption == 'Ja') {
                this.deleteBooking(item)
              }
            })
          }

          if (result.index == OPTION_ARRIVE) {
            this.plannerService.setReservationStatus( { reservationNumber : this.selectedBooking.reservation,
                                                        administration : this.administrationService.selectedAdministration
                                                      }).then(data => {
              if (data) {
                this.getPlannerData(this.accommodation(), false)
              }
            } );

          }
        })
      }
      else {
        this.dialogService.showMessage('Deze boeking kunt u niet aanpassen', 'Sportplanner')
      }

    }
    else
    if (item.type == enum_planner_closed) {
      //console.log('SCENARIO 3');

      this.dialogService.showMessage('Hier kan niet geboekt worden\n\nDe baan is gesloten', 'Sportplanner')
    }
    else
    if (item.type == enum_planner_blockbooking) {
      //console.log('SCENARIO 3a');

      this.dialogService.showMessage('Hier kan niet geboekt worden\n\nDe baan is geblokkeerd', 'Sportplanner')
    }
    else {
      //console.log('SCENARIO 4');

      if (item.type == enum_planner_booking) {
        this.selectedBooking = item;

        this.rightClick(item);
      }
      else {
        //console.log('MULTI-SELECT CLOSED CHECK');
      }
    }
  }

  cancel(): void {
    this.plannerService.mode = 'browse';
    this.targetItem = undefined;
    this.selectedBooking = undefined;
  }

  save(): void {
    var _parameters = { accommodation : this.accommodation(),
                        date : this.plannerService.selectedDate,
                        itemStart : this.targetItem,
                        itemEnd : this.hoverItem
                      }

    this.plannerService.addBooking(_parameters)
      .then(result => {
        //console.log(JSON.stringify(result));
        this.afterSave(result.recordsets[result.recordsets.length - 1][0].result);
        this.getPlannerData(this.accommodation(), false)
      });
  }

  selectionHasItemsOfType(type) : boolean {
    var _closed = false;

    //controleer als er een multi-select gedaan is, of er gesloten items tussen zitten...
    if (this.startItem && this.hoverItem && this.startItem != this.hoverItem) {
      var _roomStart = Math.min(this.startItem ? this.startItem.room.number : 999999, this.hoverItem ? this.hoverItem.room.number : 999999);
      var _roomEnd = Math.max(this.startItem ? this.startItem.room.number : 0, this.hoverItem ? this.hoverItem.room.number : 0);
      var _timeStart = new Date(Math.min(this.startItem ? new Date(this.startItem.timeStart).getTime() : 9999999999999, this.hoverItem ? new Date(this.hoverItem.timeStart).getTime() : 9999999999999));
      var _timeEnd = new Date(Math.max(this.startItem ? new Date(this.startItem.timeEnd).getTime() : 0, this.hoverItem ? new Date(this.hoverItem.timeEnd).getTime() : 0));

      for (var _room = _roomStart; _room <= _roomEnd; _room++) {
        for (var rg = 0; rg < this.roomGroups.length; rg++) {
          for (var r = 0; r < this.roomGroups[rg].rooms.length; r++) {
            if (this.roomGroups[rg].rooms[r].number == _room) {

              while (!_closed && _timeStart <= _timeEnd) {
                if (this.getType(this.roomGroups[rg].rooms[r], _timeStart) == type) {
                  _closed = true;
                }

                _timeStart = new Date(_timeStart.getTime() + this.accommodation().timeLineInterval * 1000);
              }
            }
          }
        }
      }
    }

    return _closed;
  }

  afterSave(result : boolean) : void {
    if (result) {
      this.plannerService.mode = 'browse';
      this.refreshPlanner();
    }
  }

  over(item): void {
    /*
    Als je multi-select mag doen en een selectie gemaakt hebt (this.startItem = gevuld)
    dan wil je alleen dat de hoverItem wijzigt op het moment dat je de muis nog ingedrukt hebt
    */

    if (!this.startItem || this.isDown) {
      this.hoverItem = item;
      this.setColors();
    }
  }

  setColors() {
    this.setColorsCount++;

    var _roomStart = Math.min(this.startItem ? this.startItem.room.number : 999999, this.hoverItem ? this.hoverItem.room.number : 999999);
    var _roomEnd = Math.max(this.startItem ? this.startItem.room.number : 0, this.hoverItem ? this.hoverItem.room.number : 0);
    var _timeStart = new Date(Math.min(this.startItem ? new Date(this.startItem.timeStart).getTime() : 9999999999999, this.hoverItem ? new Date(this.hoverItem.timeStart).getTime() : 9999999999999));
    var _timeEnd = new Date(Math.max(this.startItem ? new Date(this.startItem.timeEnd).getTime() : 0, this.hoverItem ? new Date(this.hoverItem.timeEnd).getTime() : 0));

    if (this.plannerService.user.hasRight('SelectMultipleTimes') || this.plannerService.user.hasRight('SelectMultipleObjects')) {
      for (var t = 0; t < this.times.length; t++) {
        for (var i = 0; i < this.times[t].items.length; i++) {
          var _item = this.times[t].items[i];

          var _selected = false;

          if (this.startItem && this.hoverItem) {                            /* er zijn meerdere items geselecteerd */

            if (this.startItem.reservation) {     /* dit is een reservering --> niet selecteren! */  
              
            }
            else
            if (this.plannerService.user.hasRight('SelectMultipleTimes') && this.plannerService.user.hasRight('SelectMultipleObjects')) {
              _selected = ((_item.timeStart >= _timeStart)   && (_item.timeEnd <= _timeEnd) &&
                           (_item.room.number >= _roomStart) && (_item.room.number <= _roomEnd));
            }
            else {
              if (this.plannerService.user.hasRight('SelectMultipleTimes')) {
                _selected = ((_item.timeStart >= _timeStart) && (_item.timeEnd <= _timeEnd));
              }
              else
              if (this.plannerService.user.hasRight('SelectMultipleObjects')) {
                _selected = ((_item.room.number >= _roomStart) && (_item.room.number <= _roomEnd));
              }
            }
          }

          _item.isSelected = _selected && !_item.reservation;
          _item.color = _item.isSelected ? 'yellow' : _item.colorOriginal;
        }
      }
    }
  }

  isParticipant(item, code) : boolean {
    var _result = false;

    for (var p = 0; p < item.participants.length; p++) {
      if (item.participants[p].code == code) {
        _result = true;
      }
    }

    return _result;
  }

  selectDate(event: MatDatepickerInputEvent<Date>) {
    this.plannerService.selectedDate = new Date(event.value);

    this.refreshPlanner();
  }

  mouseup(event, item) : void {
    this.isDown = false;
  }

  contextMenu(event, item ) {
    this.rightClick(item);

    return false;
  }

  mousedown(event, item) {
    if (event.which === 1 ) { //linker muisknop
      this.isDown = true;

      if (this.plannerService.user.hasRight('SelectMultipleTimes') || this.plannerService.user.hasRight('SelectMultipleObjects')) {
        this.startItem = item;
        this.hoverItem = item;
      }

      this.setColors();
    }
  }

  roomSelected(room) : boolean {
    if (this.hoverItem) {
      return (this.hoverItem.room == room);
    }
    else {
      return false;
    }
  }

  timeSelected(time : Date) : boolean {
    if (this.hoverItem) {
      return (this.rlxDate.timeToString(this.hoverItem.timeStart) == this.rlxDate.timeToString(time));
    }
    else {
      return false;
    }
  }

  refreshPlanner() {
    this.getPlannerData(this.accommodation(), false);
  }

  selectionToText() : string {
    var _result = '';

    var _timeStart = new Date(Math.min(new Date(this.startItem.timeStart).getTime(), new Date(this.hoverItem.timeStart).getTime()));
    var _timeEnd = new Date(Math.max(new Date(this.startItem.timeEnd).getTime(), new Date(this.hoverItem.timeEnd).getTime()));
    var _roomStart = Math.min(this.startItem ? this.startItem.room.number : this.hoverItem.room.number, this.hoverItem.room.number);
    var _roomEnd = Math.max(this.startItem ? this.startItem.room.number : this.hoverItem.room.number, this.hoverItem.room.number);

    if (_roomStart == _roomEnd) {
      _result = 'baan ' + _roomStart
    }
    else
    if (_roomEnd - _roomStart == 1) {
      _result = 'baan ' + _roomStart + ' en ' + _roomEnd
    }
    else {
      _result = 'baan ' + _roomStart + ' t/m ' + _roomEnd
    }

    _result = _result + ' van ' + this.rlxDate.timeToString(_timeStart) + ' tot ' + this.rlxDate.timeToString(_timeEnd) + ', totaal '

    if (_roomStart == _roomEnd) {
      _result = _result + '1 baan'
    }
    else {
      _result = _result + (_roomEnd - _roomStart + 1) + ' banen'
    }
  
    _result = _result + ' ' + this.rlxDate.timeUTCToString(new Date(_timeEnd.getTime() - _timeStart.getTime())) + ' uur';

    _result = _result + ' (' + this.rlxDate.timeUTCToHourString(new Date((_roomEnd - _roomStart + 1) * (_timeEnd.getTime() - _timeStart.getTime()))) + ' uur)';

    return _result;
  }

  toggleSetup() {
    this.isSetup = !this.isSetup;
  }

  timeLineLeftChange(event) {
    this.accommodation().showTimeLineLeft = (event.checked);
    this.refreshPlanner();
  }

  timeLineRightChange(event) {
    this.accommodation().showTimeLineRight = (event.checked);
    this.refreshPlanner();
  }

  timeLineShiftChange(event) {
    this.accommodation().showTimeLineShift = (event.checked);
    this.refreshPlanner();
  }

  debugCaption() : string {
    if (this.isDebug) {
      return "DEBUG"
    }
    else {
      return ""
    }
  }

  clickParticipant(object, option) : void {
    if (object.dialogService) {
      object.dialogService.showMessage(option.caption, 'Planner');
    }
  }

  displayParticipants(item) : void {
    if (item) {
      if (this.plannerService.user.hasRight('ShowReservations') && !this.showParticipants) {
        var _participants = []
      
        for (var p = 0; p < item.participants.length; p++) {
          _participants.push({caption: item.participants[p].name});
        }

        this.dialogService.optionBox('Groepsles van ' + this.rlxDate.timeToString(item.timeStart) + ' tot ' + this.rlxDate.timeToString(item.timeEnd) + '\r\n\r\n' +
                                     'Gegeven door: ' + item.relations[0].name, 'Deelnemers', _participants, this, this.clickParticipant);
      }
    }
  }

  selectBooking(item) : void {
    if (this.selectedBooking) {
      this.selectedBooking.isSelectedBooking = false;
    }

    if (item && item.type == enum_planner_booking) {
      item.isSelectedBooking = true;
      this.selectedBooking = item
      this.plannerService.mode = 'browse';
    }
    else {
      if (this.isValidTarget(item)) {
        this.targetItem = item;
      }
    }
  }

  deleteBooking(item) : void {
    if (this.allowDelete(item)) {
      this.plannerService.deleteBooking({ administration : this.administrationService.selectedAdministration, accommodation : this.accommodation(), item :item }).then(result => {
        this.selectedBooking = undefined;
        this.getPlannerData(this.accommodation(), false)
      });
    }
    else {
      this.dialogService.showMessage('U mag deze boeking niet verwijderen!', 'Sportplanner');
    }
  }

  allowMove(item) : boolean {
    var _result = false;

    if (this.isOwnBooking(item)) {
      _result = this.plannerService.user.hasRight("EditReservation")
    }
    else {
      _result = this.plannerService.user.hasRight("EditReservationOnBehalf")
    }

    return _result;
  }

  allowCopy(item) : boolean {
    var _result = false;

    if (this.isOwnBooking(item)) {
      _result = this.plannerService.user.hasRight("RepeatReservation")
    }
    else {
      _result = this.plannerService.user.hasRight("RepeatReservationOnBehalf")
    }

    return _result;
  }

  allowDelete(item) : boolean {
    var _result = false;

    if (this.isOwnBooking(item)) {
      _result = this.plannerService.user.hasRight("DeleteReservation")
    }
    else {
      _result = this.plannerService.user.hasRight("DeleteReservationOnBehalf")
    }

    return _result;
  }

  isOwnBooking(item) : boolean {
    var _result = false;

    if (item) {
      if (item.relations) {
        if (item.relations.length) {
          _result = item.relations[0].code == this.plannerService.user.code;
        }
      }
    }


    return _result
  }

  isSelected(item) : boolean {
    var _result = false;

    if (item) {
      var _roomStart = Math.min(this.startItem ? this.startItem.room.number : 999999, this.hoverItem ? this.hoverItem.room.number : 999999);
      var _roomEnd = Math.max(this.startItem ? this.startItem.room.number : 0, this.hoverItem ? this.hoverItem.room.number : 0);
      var _timeStart = new Date(Math.min(this.startItem ? new Date(this.startItem.timeStart).getTime() : 9999999999999, this.hoverItem ? new Date(this.hoverItem.timeStart).getTime() : 9999999999999));
      var _timeEnd = new Date(Math.max(this.startItem ? new Date(this.startItem.timeEnd).getTime() : 0, this.hoverItem ? new Date(this.hoverItem.timeEnd).getTime() : 0));

      _result = (item.room.number >= _roomStart && item.room.number <= _roomEnd && item.timeStart >= _timeStart && item.timeEnd <= _timeEnd);
    }

    return _result;
  }

  gotoLogin() {
    this.router.navigate(['/dashboard'])
  }

  startMove(item) {


    if (this.plannerService.mode == 'move') {
      var _move_parameters = { administration : this.administrationService.selectedAdministration,
                          accommodation: this.accommodation(),
                          date: this.plannerService.selectedDate,
                          source : this.selectedBooking, 
                          destination : item, 
                          mode : this.plannerService.mode,
                        }      

      this.plannerService.moveBooking(_move_parameters).then(result => {
        var _batch = this.batchService.evaluateBatch(result, BATCH.REST_BOOKING)

        if (_batch.success) {
          this.snackBar.open(_batch.message, 'ok', {duration: 2500});

          this.getPlannerData(this.accommodation(), false);
        }
        else {
          this.snackBar.open(_batch.message, 'FOUT', {duration: 2500});
          }
      })
    }
    else if (this.plannerService.mode = 'copy') {
      var _copy_parameters = { accommodation: this.accommodation(),
                                date: this.plannerService.selectedDate,
                                itemStart: item,
                                itemEnd: item
                              }


      this.plannerService.addBooking(_copy_parameters).then(result => {
                  //console.log(JSON.stringify(result));

        var _batch = this.batchService.evaluateBatch(result, BATCH.REST_BOOKING)

        if ( _batch.success ) {
          this.snackBar.open(_batch.message, 'ok', {duration: 2500});
       
          this.getPlannerData(this.accommodation(), false);
        }
        else {
          this.snackBar.open(_batch.message, 'FOUT', {duration: 2500});
        }
      })
    }



    // let dialogRef = this.dialog.open(DialogVerplaatsen, { width: '640px', data: { mode: this.plannerService.mode, planner: this, source : this.selectedBooking, destination: item}});

    // dialogRef.afterClosed().subscribe(result => {
    //   this.plannerService.mode = 'browse';
    //   this.selectedBooking = undefined;
    //   this.targetItem = undefined;
    //   this.getPlannerData(this.accommodation(), true);
    // });
  }

  allowShowParticipants(item) : boolean {
    var _result = /*this.showParticipants &&*/ this.plannerService.user.hasRight('ShowReservations');


    //tenzij de ingelogde gebruiker onderdeel is van de reservering, dan wil je de namen wel tonen
    if (!_result) {
      if (item && !item.isGroup) {  //maar dan niet bij een groepsles, je mag niet de namen van andere deelnemers zien...

        var _participants = this.getParticipants(item.reservation, item.reservationDetailNumber);

        for (var p = 0; p < _participants.length; p++) {
          if (_participants[p].code == this.plannerService.user.code) {
            _result = true;
          }
        }
      }
    }

    return _result;
  }

  bookingToHTML(item) : string {
    var _result =  '';

    _result = '<b>' + this.rlxDate.timeToString(item.timeStart) + '-' + this.rlxDate.timeToString(item.timeEnd) + '</b>';
    _result = _result + '<br>';

    for (var p = 0; p < item.participants.length; p++) {
      if (p > 0) {
        _result = _result + ', ';
      }

      _result = _result + item.participants[p].name;
    }


    return _result;
  }

  isValidTarget(item) : boolean {
    if (this.isDebug) { console.log('item = ' + JSON.stringify(item)) }

    if (!item) { return false }
    if (this.plannerService.mode != 'move' && this.plannerService.mode != 'copy') { return false }
    if (item.type == enum_planner_booking) { return false }
    if (item.type == enum_planner_closed) { return false }
    if (item.type == enum_planner_blockbooking) { return false }
    if (this.rlxDate.isHistory(item.timeStart)) { return false }

    return true;
  }

  getStatusCaption(item) : string {
    if (item.status == STATUS_EXPECTED) {
      return 'V'
    }
    else
    if (item.status == STATUS_ARRIVED) {
      return 'B'
    }
    else {
      return 'O'
    }
  }

  hintText() : string {
    var _result = 'boeking van:';

    if (this.selectedBooking) {
      _result = _result + ' <b>' + this.selectedBooking.participants[0].name + '</b> om ' +
                '<b> ' + this.rlxDate.timeToString(this.selectedBooking.timeStart) + '</b> op baan <b>' + this.selectedBooking.room.number + '</b><BR>';
    }

    if (this.targetItem) {
      _result = _result + 'naar: <b>' +  this.rlxDate.timeToString(this.targetItem.timeStart) + '</b> op baan <b>' + this.targetItem.room.number + '</b>';
    }

    return _result;
  }

  bookingArrived(item) {
    if (this.rlxDate.isSameTime(this.rlxDate.dateOnly(new Date()), this.rlxDate.dateOnly(item.timeStart))) { //is het een boeking van vandaag?
      if (!this.rlxDate.isHistory(item.timeStart)) { //vandaag in de toekomst?
        if (item.status == STATUS_EXPECTED) {
          let _question = 'Wilt u deze spelers binnenmelden?'

          let _dialog = this.dialogService.confirm(_question, 'Binnenmelden');

          _dialog.afterClosed().subscribe(result => {
            if (result.caption == 'Ja') {
              this.plannerService.setReservationStatus( { reservationNumber : item.reservation,
                                                          administration : this.administrationService.selectedAdministration
                                                        }).then(data => {
                if (data) {
                  this.getPlannerData(this.accommodation(), false)
                }
              } );
            }
          })
        }
      }
    }
  }
}
