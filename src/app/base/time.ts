import { Booking } from './booking';

export class Time {
  items : Booking[];
  time : Date;
  isStart : boolean;
}
