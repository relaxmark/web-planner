import { Relation } from './relation';
import { Activity } from './activity';

export class Person {
  code : string;
  name : string;
  job : string;
  relation : Relation;
}

export class Group {
  code : string;
  name : string;
  order : number;
}

export class Role {
  name : string;
}

export class PersonGroup {
  person : Person;
  group : Group;
  order : number;
}

export class PersonRole {
  person : Person;
  role : Role;
}

export class PersonActivity {
  person : Person;
  activity : Activity;
}

export class Organization {
  code : string;
  name : string;
  order : number;

  persons : Person[];
  groups : Group[];
  roles : Role[];

  personGroups : PersonGroup[];
  personRoles : PersonRole[];
  personActivities : PersonActivity[];
}

