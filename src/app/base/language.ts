export class Language {
  code : string;
  name : string;
}

export class Translation {
  value : string;
  language : Language;
}

export class TextFragment {
  original : string;
  translations : Translation[];
}
