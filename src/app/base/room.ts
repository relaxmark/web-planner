export class Room {
  name: string;
  member: boolean;
  number: number;
  startTime: Date;
  endTime: Date;
  bookingInterval: string;
  timeLineInterval: string;
  enumIndex: number;
}

export class RoomDetail {
  name: string;
  member: boolean;
  number: number;
  times: RoomTime[];
}

export class RoomTime {
  startTime: Date;
  endTime: Date;
  bookingInterval: Date;
  timeLineInterval: Date;
  enumIndex: number;
}

export class RoomGroup {
  name : string;
  rooms : RoomDetail[];
}

export class ObjectGroup {
  accommodationCode : string;
  name : string;
}
