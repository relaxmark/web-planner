import { RoomDetail } from './room';
import { Relation } from './relation';


export class Booking {
  relation: number;
  reservation: string;
  reservationDetailNumber : string;
  caption: string;
  type: number;
  room: RoomDetail;
  timeStart: Date;
  timeEnd: Date;
  color: string;
  fontColor: string;
  colorOriginal : string;
  rowSpan : number;
  colSpan : number;
  isPartial : boolean;
  isSelected : boolean;
//  isHover  : boolean;
  isHistory : boolean;
  timeString : string;
  relations : Relation[];
  participants : Relation[];
  isGroup : boolean;
  isSelectedBooking : boolean;
  showTime : boolean;
  status : number;
  statusCode : string;
}

export class BookingColor {
  mode : string;
  font : string;
  background : string;
}
