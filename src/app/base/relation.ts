export class Right {
  type : string;
  name : string;
  index : number;
  assigned : boolean;
}

export class User {
  code : number;
  name: string;
  email: string;
  password: string;
  roleName: string;

  rights : Right[];

  hasRight: (name) => boolean;

  constructor (code: number, name: string, email: string, password: string) {
    this.code = code;
    this.name = name;
    this.email = email;
    this.password = password;
    this.roleName = "";
    this.rights = [];


    this.hasRight = function(name) : boolean {
      var _result = false;

      if (this.rights && this.rights.length) {
        for (var r = 0; r < this.rights.length; r++) {
          if (this.rights[r].name.toLowerCase() == name.toLowerCase()) {
            _result = this.rights[r].assigned;
          }
        }
      }

      return _result;
    }
  }
}

export class Relation {
  code : number;
  name : string;
  isMale : boolean;
}
