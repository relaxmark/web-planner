export class BatchResult {
  success : boolean;
  message : string;
  errorCode : number;
}
