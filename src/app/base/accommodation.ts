export class Accommodation {
  nameFull : string;
  nameShort : string;
  timeLineInterval: number;
  bookingInterval: number;

  showTimeLineLeft : boolean;
  showTimeLineRight : boolean;
  showTimeLineShift : boolean;
  showTimeInCell : boolean;

}
