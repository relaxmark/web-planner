export class Activity {
  name : string;
  code : string;
  groupCode : string;
  type : string;

  fontColor : string;
  backColor : string;

  minAttendees : Number;
  maxAttendees : Number;
}

export class ActivityGroupObjectGroup {
  activityGroup : string;
  objectGroup : string;
}
