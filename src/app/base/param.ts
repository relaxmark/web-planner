export class Param {
  moduleName : string;
  groupName : string;
  paramName : string;
  paramValue : any;
}
