import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'rlx-dialog',
  templateUrl: 'rlx-dialog.html',
  styleUrls: [ './rlx-dialog.css' ],
  encapsulation: ViewEncapsulation.None,
})

export class RlxDialog {
  buttons = [];
  caption = '';
  message = '';
  options = [];
  parent = undefined;

  constructor(
    public dialogRef: MatDialogRef<RlxDialog>,

    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.buttons = data.buttons;
      this.caption = data.caption;
      this.message = data.message;
      this.options = data.options;
      this.parent = data.parentObject;
    }

  onClick(button) {
    this.dialogRef.close(button);
  }

  onOption(parent, option) {
    if (this.data.onClick) {
      this.data.onClick(parent, option);
    }
    else {
      this.dialogRef.close(option);
    }
  }
}
