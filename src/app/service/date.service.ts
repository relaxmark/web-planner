import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DateDialog } from '../dialog/date.dialog'

@Injectable()
export class DateService {

  constructor( public dialog: MatDialog ) { }

  dateOnly(time) {
    var _date = new Date(time);

    _date = new Date(_date.getFullYear() + '-' + (_date.getMonth() + 1) + '-' + _date.getDate());

    return _date;
  }

  today() : Date {
    return this.dateOnly(new Date());
  }

  isToday(time : Date) : boolean {
    return this.dateOnly(time).getTime() == this.dateOnly(new Date()).getTime();
  }

  isHistory(time : Date) : boolean {
    var _now = new Date();

    return time < _now;
  }

  dateToString(date : Date) : string {
    return date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
  }

  dateToJSString(date : Date) : string {
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  }

  timeToString(time : Date) : string {
    return time.getHours() + ':' + (time.getMinutes()<10?'0':'') + time.getMinutes();
  }

  timeUTCToString(time : Date) : string {
    return time.getUTCHours() + ':' + (time.getUTCMinutes()<10?'0':'') + time.getUTCMinutes();
  }

  timeUTCToHourString(time : Date) : string {
    return (this.getDays(time) * 24) + time.getUTCHours() + ':' + (time.getUTCMinutes()<10?'0':'') + time.getUTCMinutes();
  }

  isSameTime(day1 : Date, day2 : Date) : boolean {
    return day1.getTime() == day2.getTime();
  }

  getDays(time : Date) : number {
    var _result = -1;

    var _value = time.getTime();

    while (_value >= 0) {
      _result++;

      _value = _value - 24*60*60*1000;
    }

    return _result
  }

  addDays(time : Date, days : number) : Date {
    return new Date(time.getTime() + 24*60*60*1000*days)
  }

  addSeconds(time : Date, seconds : number) : Date {
    return new Date(time.getTime() + 1000*seconds)
  }

  addHours(time : Date, hours : number) : Date {
    return new Date(time.getTime() + 60*60*1000*hours)
  }

  toSeconds(time : Date) : number {
    var _time : string = this.timeToString(time);
    var _values = _time.split(':');

    if (_values.length == 2) {
      return (+_values[0]) * 3600 + (+_values[1]) * 60;
    }
    else {
      return (+_values[0]) * 3600 + (+_values[1]) * 60 + (+_values[2]);
    }
  }

  pickDate(date) : any {
    let dialogRef = this.dialog.open(DateDialog, { width: '480px', data: { 'date': date }});

    return dialogRef;
  }
}
