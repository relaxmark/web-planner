import { Injectable } from '@angular/core';
import { PlannerService } from './planner.service';
import { RelationService } from './relation.service';
import { OrganizationService } from './organization.service';
import { BatchService } from './batch.service';
import { ParamService } from './param.service';

import { Administration } from '../base/administration'

import { CookieService } from 'ngx-cookie';


@Injectable()
export class AdministrationService {
  administrations : Administration[] = [];
  selectedAdministration : Administration;

  constructor(
    private plannerService : PlannerService,
    private relationService : RelationService,
    private organizationService : OrganizationService,
    private paramService : ParamService,
    private cookieService: CookieService,
    private batchService: BatchService ) {
      //console.log('AdministrationService.Create');


      this.plannerService.getAdministrations()
        .then(data => this.setAdministrations(data));
    }

  setAdministrations(data) {
    if (data) {
      var _recordset = this.batchService.getRecordSet(data, ['number', 'name', 'language']);

      if (_recordset) {
        this.administrations = _recordset;

        var _adm = this.cookieService.getObject('adm');

        //console.log('COOKIE OPGEHAALD --> ' + JSON.stringify(_adm))

        if (_adm) {
          this.selectAdministration(_adm);
        }
      }
      else {
        console.error('AdministrationService.setAdministrations - geen geldige recordset gevonden')
      }
    }
    else {
      this.plannerService.getAdministrations().then(data => this.setAdministrations(data));
    }
  }

  selectAdministration(adm) {
    //console.log('AdministrationService.selectAdministration(' + JSON.stringify(adm) + ')')

    this.selectedAdministration = adm;

    this.cookieService.putObject('adm', adm);

    this.plannerService.getAccommodations(adm).then(data => {
      this.setAccommodations(data);
    });

    this.plannerService.getRelations(adm).then(data => this.relationService.setData(data));
    this.plannerService.getOrganization(adm).then(data => this.organizationService.setOrganization(data));
    this.plannerService.getParams(adm, undefined).then(data => this.paramService.setData(data));  //TODO : we weten nog niet welke user we zijn hier...na inloggen nogmaals params uitlezen!
  }

  administrationByNumber(index : number) : Administration {
    var _administration;

    for (var a = 0; a < this.administrations.length; a++) {
      if (this.administrations[a].number == index) {
        _administration = this.administrations[a];
      }
    }

    return _administration;
  }


  //verhuizen naar accommodation-service
  setAccommodations(data) {
    if (data) {
      var _accommodations = this.batchService.getRecordSet(data, ['nameFull', 'nameShort', 'timeLineInterval', 'bookingInterval', 'showTimeLineLeft', 'showTimeLineRight', 'showTimeLineShift', 'showTimeInCell']);    
      
      if (_accommodations) {
        this.plannerService.accommodations = _accommodations;

        if (_accommodations.length) {
          this.plannerService.selectedAccommodation = _accommodations[0];
        }
      } 
      else {
        console.error('AdministrationService.setAccommodations - geen accommodaties gevonden')
      }
    }
    else {
      this.selectAdministration(this.selectedAdministration);
    }
  }


}
