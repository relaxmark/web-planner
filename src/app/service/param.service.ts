import { Injectable } from '@angular/core';
import { Param } from '../base/param';
import { BatchService } from './batch.service';


@Injectable()
export class ParamService {

  params : Param[];

  constructor( private batchService : BatchService ) { }

  setData(data) {
    //console.log('ParamService.setData(' + JSON.stringify(data) + ')');
    var _values = []

    var _parameters = this.batchService.getRecordSet(data, ['groupName', 'paramName', 'paramValue', 'moduleName']);

    if (_parameters) {
      for (var p = 0; p < _parameters.length; p++) {
        _values.push(_parameters[p])
      }
    }

    this.params = _values;

    //console.log('PARAMS : ' + JSON.stringify(this.params));
  }

  getParam(param : Param) : Param {
    var _result = { moduleName: undefined, groupName: undefined, paramName: undefined, paramValue: undefined };

    if (this.params) {
      for (var p = 0; p < this.params.length; p++) {
        if (this.params[p].moduleName == param.moduleName &&
            this.params[p].groupName == param.groupName &&
            this.params[p].paramName == param.paramName) {
          _result = this.params[p];
        }
      }
    }
    return _result;
  }
}
