import { Injectable } from '@angular/core';
import { BatchService } from './batch.service';
import { PlannerService } from './planner.service';
import { Relation } from '../base/relation';

@Injectable()
export class RelationService {

  relations : Relation[] = [];

  constructor( private plannerService : PlannerService,
               private batchService : BatchService ) { }

  setData(data) {
    this.relations = [];

    var _relations = this.batchService.getRecordSet(data, ['code', 'name', 'isMale']);

    if (_relations) {
      this.relations = _relations;
    }
  }
}
