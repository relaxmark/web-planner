import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { BatchResult } from '../base/batch';

export class BATCH {
    public static REST_LOGIN = 1
    public static REST_ACCOMMODATIONS = 2;
    public static REST_ROOMS = 3;
    public static REST_ADMINISTRATIONS = 4;
    public static REST_BOOKING = 5;
    public static REST_PARTICIPANT = 6;
    public static REST_RELATIONS = 7;
    public static REST_ORGANIZATION = 9;
    public static REST_PRICE = 10;
    public static REST_ACTIVITY = 11;
    public static REST_PARAMETERS = 12;
    public static REST_ACCOUNT = 13;
    public static REST_RESERVATION_STATUS = 14;
}

@Injectable()
export class BatchService {

  constructor( public dialog: MatDialog ) { }

  getRecordSet(data, fields) : any {
    var _recordset = undefined;

    var _fieldcheck = false

    if (fields) {
      if (fields.length) {
        var _fieldcheck = true
      }
      else {
        console.log('BatchService.getRecordSet - fields-property empty')
      }
    }
    else {
      console.log('BatchService.getRecordSet - fields-property undefined')
    }

    if (!_fieldcheck) {
      return _recordset;
    }

    if (data) {
      if (data.recordsets) {
        if (data.recordsets.length) {
          for (var r = 0; r < data.recordsets.length; r++) {
            if (data.recordsets[r].length) {
               _fieldcheck = true;

              for (var f = 0; f < fields.length; f++) {
                _fieldcheck = _fieldcheck && data.recordsets[r][0].hasOwnProperty(fields[f])  
              }

              if (_fieldcheck) {
                _recordset = data.recordsets[r];
              }
            }
          }
        } 
        else {
          console.log('BatchService.getRecordSet - recordsets-property empty')
        }
      } 
      else {
        console.log('BatchService.getRecordSet - recordsets-property not found')
      }
    }
    else {
      console.log('BatchService.getRecordSet - data undefined')
    }

    return _recordset
  }

  evaluateBatch(result, batch) : BatchResult {
    var _result = { success : false, message : '', errorCode : -1 };

    if (result) {
      if (result.recordsets) {                                            //bestaat de recordsets-property?
        if (result.recordsets.length) {                                   //is er een of meerdere recordset(s) teruggegeven?

          var _recordset = undefined;

          for (var i = 0; i < result.recordsets.length; i++) {
            if (result.recordsets[i]) {
              //console.log(JSON.stringify(result.recordsets[i]))

              if (result.recordsets[i].length) {
                if (result.recordsets[i][0].hasOwnProperty('ProcedureResult') ) {
                  _recordset = result.recordsets[i]
                    
                  //Als IdentifierResultset meegestuurd is, dan wordt daar "PR<BATCH_ID>" in verwacht.
                  //Dus als je gaat inloggen (REST_LOGIN = 1), dan IdentifierResultset = "PR1"
                  if (result.recordsets[i][0].hasOwnProperty('IdentifierResultset') ) {
                    if (result.recordsets[i][0].IdentifierResultset != 'PR' + batch) {
                      _recordset = undefined
                    }
                  }
                }
              }
            }
          }

          if (_recordset) {
            if (_recordset.length) {
              _result.message = _recordset[0].ProcedureResultInfo
              _result.errorCode = _recordset[0].ProcedureErrorCode

              if (_recordset[0].ProcedureResult == 1) {   //check de result-property van de laatste recordset
                _result.success = true;
              }
            }
            else {
              console.error('BATCH : ANTWOORD LEEG')
            }
          }
          else {
            console.error('BATCH : ANTWOORD ONTBREEKT')
          }
        }
        else {
          console.error('BATCH : DATA LEEG')
        }
      }
      else {
        console.error('BATCH : DATA ONTBREEKT')
      }
    }
    else {
      console.error('BATCH : GEEN ANTWOORD')
    }

    return _result;
  }

  returnError(batch, errorCode, message) : Promise<any> {
    return Promise.resolve({recordsets : [[{ProcedureResult : -2, IdentifierResultset : 'PR' + batch, ProcedureErrorCode : errorCode, ProcedureResultInfo : message}]]})

  }
}
