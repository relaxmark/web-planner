import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';

import { CookieService } from 'ngx-cookie';

import { Administration } from '../base/administration';
import { Accommodation } from '../base/accommodation';
import { Booking} from '../base/booking';
import { Room} from '../base/room';
import { Time } from '../base/time';
import { User, Relation } from '../base/relation';
import { BATCH, BatchService } from './batch.service';

const ACTION_ADD            = 'ADD';
const ACTION_DELETE         = 'DELETE';
const ACTION_MOVE           = 'MOVE';
const ACTION_RESEND         = 'RESEND';
const ACTION_LOGIN          = 'LOGIN';
const ACTION_INQUIRY        = 'INQUIRY';
const ACTION_PASSWORD_LOST  = 'PASSWORD_LOST'
const ACTION_PASSWORD_RESET = 'PASSWORD_RESET'

const STATUS_ARRIVED = 3;

@Injectable()
export class PlannerService {
  timedOut = false;
  minDate = new Date();

  selectedDate = new Date();

  selectedAccommodation : Accommodation;

  user: User = new User(0, '', '', '');

  accommodations : Accommodation[] = [];
  //relations : Relation[] = [];

  mode = 'browse';

  private restUrl = 'http://rlx-planner.azurewebsites.net/framework/batch';

  constructor(
    private httpClient: HttpClient,
    private cookieService: CookieService,
    private batchService: BatchService,
    public snackBar: MatSnackBar, ) {
      //console.log('PlannerService.Create');

      this.user = this.cookieService.getObject('user') as User;

      //console.log('user = ' + JSON.stringify(this.user));

      if (!this.user) {
        this.user = new User(0, '', '', '');
      }
    }

  timeToUTCString(time : Date) : string {
    return time.getUTCHours() + ':' + (time.getUTCMinutes()<10?'0':'') + time.getUTCMinutes();
  }

  httpOptions() : any {
    return {headers: new HttpHeaders({'Access-Control-Allow-Origin': '*', 'Content-Type': 'application/json'}), responseType: 'json'};
  }

  addBooking(parameters) : Promise<any> {
    //gebruik de ingelogde relatie als default
    var code1 = (parameters.relations.length > 0 ? (parameters.relations[0] ? parameters.relations[0].code : this.user.code) : "");  
    var code2 = (parameters.relations.length > 1 ? (parameters.relations[1] ? parameters.relations[1].code : "") : "");

    //standaard boeking is paars met wit
    var colorBackground = (parameters.activity ? parameters.activity.backColor : "purple");
    var colorFont = (parameters.activity ? parameters.activity.fontColor : "white");

    var _minimum = (parameters.minimum ? parameters.minimum : 0);
    var _maximum = (parameters.maximum ? parameters.maximum : 0);

    var _attendees = '';

    var _activityCode = parameters.activity ? parameters.activity.code : '';
    var _activityGroupCode = parameters.activity ? parameters.activity.groupCode : '';

    for (var a = 0; a < parameters.attendees.length; a++) {
      if (_attendees != '') {
        _attendees = _attendees + ',';
      }

      _attendees = _attendees + parameters.attendees[a].code;
    }

    var input = '{"action":"' + BATCH.REST_BOOKING + '", ' +
                 '"params":{"Action" : "' + ACTION_ADD + '", ' +
                           '"Administration" : "' + parameters.administration.number + '", ' +
                           '"AccommodationShortName":"' + parameters.accommodation.nameShort + '", ' +
                           '"DateStartTime":"' + this.jsonDate(parameters.date) + ' ' + this.timeToUTCString(parameters.itemStart.timeStart) + '", ' +
                           '"DateEndTime":"' + this.jsonDate(parameters.date) + ' ' + this.timeToUTCString(parameters.itemEnd.timeEnd) + '", ' +
                           '"ObjectNumberStart":"' + parameters.itemStart.room.number + '", ' +
                           '"ObjectNumberEnd":"' + parameters.itemEnd.room.number + '", ' +
                           '"RelationCodeBooker":"' + code1 + '", ' +
                           '"ReservationNumber":"", ' +
                           '"ColorBack":"' + colorBackground + '", ' +
                           '"ColorFont":"' + colorFont + '", ' +
                           '"TypeOfReservation":"' + parameters.type + '", ' +
                           '"MinAttendees":"' + _minimum + '", ' +
                           '"MaxAttendees":"' + _maximum + '", ' +
                           '"Participants":"' + _attendees + '", ' +
                           '"ActivityCode":"' + _activityCode + '", ' +
                           '"ActivityGroupShortName":"' + _activityGroupCode + '"}}';

    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  moveBooking(parameters)  : Promise<any> {
  //moveBooking(source : Booking, destination : Booking, date : Date, accommodation: Accommodation)  : Promise<any> {
    console.log(parameters.mode + " : " + JSON.stringify(parameters.source) + ' --> ' + JSON.stringify(parameters.destination));

  //SANITY CHECKS
    if (!parameters) {
      return this.batchService.returnError(BATCH.REST_BOOKING, -1, 'geen parameters meegegeven aan moveBooking')
    }

    if (!parameters.source) {
      return this.batchService.returnError(BATCH.REST_BOOKING, -2, 'originele boeking onbekend')
    }

    if (!parameters.destination) {
      return this.batchService.returnError(BATCH.REST_BOOKING, -3, 'bestemming onbekend')
    }

    if (!parameters.administration) {
      return this.batchService.returnError(BATCH.REST_BOOKING, -4, 'administratie onbekend')
    }

    if (!parameters.accommodation) {
      return this.batchService.returnError(BATCH.REST_BOOKING, -5, 'accommodatie onbekend')
    }

    if (!parameters.source.relations || !parameters.source.relations.length) {
      return this.batchService.returnError(BATCH.REST_BOOKING, -6, 'relatiecode onbekend')
    }
  //EINDE SANITY CHECKS

    var input = '{"action":"' + BATCH.REST_BOOKING + '", ' +
                 '"params":{"Action" : "' + ACTION_MOVE + '", ' +
                           '"Administration" : "' + parameters.administration.number + '", ' +
                           '"AccommodationShortName":"' + parameters.accommodation.nameShort + '", ' +
                           '"DateStartTime":"' + this.jsonDate(parameters.date) + ' ' + this.timeToUTCString(parameters.destination.timeStart) + '", ' +
                           '"DateEndTime":"' + this.jsonDate(parameters.date) + ' ' + this.timeToUTCString(parameters.destination.timeEnd) + '", ' +
                           '"ObjectNumberStart":"' + parameters.destination.room.number + '", ' +
                           '"ObjectNumberEnd":"' + parameters.destination.room.number + '", ' +
                           '"RelationCodeBooker":"' + parameters.source.relations[0].code + '", ' +
                           '"ReservationNumber":"' + parameters.source.reservation + '"}}';

    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  deleteBooking(parameters) : Promise<boolean> {
  //deleteBooking(accommodation : Accommodation, item : Booking ) : Promise<boolean> {
    var input = '{"action":"' +  BATCH.REST_BOOKING + '", ' +
                 '"params":{"Action" : "' + ACTION_DELETE + '", ' +
                           '"Administration" : "' + parameters.administration.number + '", ' +
                           '"AccommodationShortName":"' + parameters.accommodation.nameShort + '", ' +
                           '"DateStartTime":"' + this.jsonDate(this.selectedDate) + ' ' + this.timeToUTCString(parameters.item.timeStart) + '", ' +
                           '"DateEndTime":"' + this.jsonDate(this.selectedDate) + ' ' + this.timeToUTCString(parameters.item.timeEnd) + '", ' +
                           '"ObjectNumberStart":"' + parameters.item.room.number + '", ' +
                           '"ObjectNumberEnd":"' + parameters.item.room.number + '", ' +
                           '"RelationCodeBooker":"' + this.user.code + '", ' +  //dit is dus de relatie die de delete-actie uitvoert, NIET de relatie uit de boeking
                           '"ReservationNumber":"' + parameters.item.reservation + '"}}';

    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  getAdministrations(): Promise<any> {
    //console.log('ADMINISTRTATIES OPHALEN');

    var input = '{"action":"' +  BATCH.REST_ADMINISTRATIONS + '", "params":{}}';

    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  getAccommodations(administration): Promise<any> {
    var input = '{"action":"' +  BATCH.REST_ACCOMMODATIONS + '", "params":{"Administration" : "' + administration.number + '"}}';

    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  accommodationByCode(code) : Accommodation {
    var _accommodation;

    for (var i = 0; i < this.accommodations.length; i++) {
      if (this.accommodations[i].nameShort === code) {
        _accommodation = this.accommodations[i];
      }
    }

    return _accommodation;
  }

  setSelectedDate(date : Date) {
    this.selectedDate = date;
  }

  jsonDate(date : Date) : string {
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()
  }

  getRooms(administration : Administration, accommodation : Accommodation): Promise<any[]> {
    var input = '{"action":"' +  BATCH.REST_ROOMS + '", "params":{"Administration" : "' + administration.number + '", "Accommodation":"' + (accommodation ? accommodation.nameShort : "") + '","Date":"' + this.jsonDate(this.selectedDate) + '"}}';

    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  doLogin(administration : Administration, relation: User, activationCode : string): Promise<any> {
    var _params = '{"Action" : "' + ACTION_LOGIN + '", ' +
                   '"Administration" : "' + administration.number + '", ' +
                   '"EmailAddress":"' + this.user.email + '", ' +
                   '"EmailPassword":"' + this.user.password + '"';

    if (activationCode) {
      _params = _params + ', ' +
                   '"ActivationCode":"' + activationCode + '"';
    }

    _params = _params + '}';

    var input = '{"action":"' + BATCH.REST_LOGIN + '", "params":' + _params + '}';

    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Oeps, er is iets fout gegaan', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getRelations(administration): Promise<any> {
    var input = '{"action":"' +  BATCH.REST_RELATIONS + '", "params":{"Administration" : "' + administration.number + '"}}';

    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  getOrganization(administration): Promise<any> {
    var input = '{"action":"' +  BATCH.REST_ORGANIZATION + '", "params":{"Administration" : "' + administration.number + '"}}';

    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  getPrice(parameters) : Promise<any> {
    var code1 = (parameters.relations.length > 0 ? (parameters.relations[0] ? parameters.relations[0].code : this.user.code) : "");  
    var code2 = (parameters.relations.length > 1 ? (parameters.relations[1] ? parameters.relations[1].code : "") : "");

    var input = '{"action":"' +  BATCH.REST_PRICE + '", ' +
                 '"params":{"Administration" : "' + parameters.administration.number + '", ' +
                           '"AccommodationShortName":"' + parameters.accommodation.nameShort + '", ' +
                           '"DateStartTime":"' + this.jsonDate(parameters.date) + ' ' + this.timeToUTCString(parameters.itemStart.timeStart) + '", ' +
                           '"DateEndTime":"' + this.jsonDate(parameters.date) + ' ' + this.timeToUTCString(parameters.itemEnd.timeEnd) + '", ' +
                           '"ObjectNumberStart":"' + parameters.itemStart.room.number + '", ' +
                           '"ObjectNumberEnd":"' + parameters.itemEnd.room.number + '", ' +
                           '"RelationCodeBooker":"' + code1 + '", ' +
                           '"RelationCodeAttendee":"' + code2 + '", ' +
                           '"ReservationNumber":"' + parameters.itemStart.reservation + '", ' +
                           '"ActivityCode":"' + parameters.activity.code + '", ' +
                           '"NumberOfAttendees":"' + (1 + (code2 ? 1 : 0)) + '"}}';

    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  getParams(administration, user) : Promise<any> {
    var userCode = '';

    if (user) {
      userCode = user.code;
    }

    var input = '{"action":"' +  BATCH.REST_PARAMETERS + '", ' +
                ' "params":{"Administration" : "' + administration.number + '",' +
                '           "User" : "' + userCode + '"' +
                '          }' +
                '}';

    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  addAccount(parameters) : Promise<any> {
      var input = '{"action":"' +  BATCH.REST_ACCOUNT + '", ' +
                ' "params":{"Administration" : "' + parameters.administration.number + '",' +
                '           "Action" : "' + ACTION_ADD + '",' +
                '           "FirstName" : "' + parameters.user.firstName + '",' +
                '           "LastName" : "' + parameters.user.familyName + '",' +
                '           "Email" : "' + parameters.user.email + '",' +
                '           "Password" : "' + parameters.user.password + '"' +
                '          }' +
                '}';


    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  getAccount(parameters) : Promise<any> {
    var input = '{"action":"' +  BATCH.REST_LOGIN + '", ' +
                ' "params":{ ' +
                     '"Action" : "' + ACTION_INQUIRY + '", ' +
                     '"ActivationCode" : "' + parameters.resendActivation + '", ' +
                     '"ResetPasswordCode" : "' + parameters.resetPassword + '"' +
                '  }' +
                '}';

    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  resendActivation(parameters) : Promise<any> {
    var input = '{"action":"' +  BATCH.REST_LOGIN + '", ' +
                ' "params":{ ' +
                     '"Administration" : "' + parameters.administration.number + '",' +
                     '"Action" : "' + ACTION_RESEND + '",' +
                     '"EmailAddress" : "' + parameters.email + '"' +
                '  }' +
                '}';

    return this.post(input);
  }

  sendLostPassword(parameters) : Promise<any> {
    var input = '{"action":"' +  BATCH.REST_LOGIN + '", ' +
                ' "params":{ ' +
                     '"Administration" : "' + parameters.administration.number + '",' +
                     '"Action" : "' + ACTION_PASSWORD_LOST + '",' +
                     '"EmailAddress" : "' + parameters.email + '"' +
                '  }' +
                '}';

    return this.post(input);
  }

  resetPassword(parameters) : Promise<any> {
    var input = '{"action":"' +  BATCH.REST_LOGIN + '", ' +
                ' "params":{ ' +
                     '"Administration" : "' + parameters.administration.number + '",' +
                     '"Action" : "' + ACTION_PASSWORD_RESET + '",' +
                     '"EmailAddress" : "' + parameters.email + '", ' +
                     '"EmailPassword" : "' + parameters.password + '"' +
                '  }' +
                '}';

    return this.post(input);
  }

  post(input) : Promise<any> {
    return this.httpClient.post(this.restUrl, input, this.httpOptions()).toPromise().then(response => response).catch(this.handleError);
  }

  addParticipant(parameters) : Promise<any> {
    var input = '{"action":"' +  BATCH.REST_PARTICIPANT + '", ' +
                ' "params":{ ' +
                     '"Administration" : "' + parameters.administration.number + '",' +
                     '"Action" : "' + ACTION_ADD + '",' +
                     '"RelationCodeParticipant" : "' + parameters.relationCode + '", ' +
                     '"ReservationNumber" : "' + parameters.reservationNumber + '", ' +
                     '"ReservationDetailNumber" : "' + parameters.reservationDetailNumber + '"' +
                '  }' +
                '}';

    return this.post(input);
  }

  deleteParticipant(parameters) : Promise<any> {
    var input = '{"action":"' +  BATCH.REST_PARTICIPANT + '", ' +
                ' "params":{ ' +
                     '"Administration" : "' + parameters.administration.number + '",' +
                     '"Action" : "' + ACTION_DELETE + '",' +
                     '"RelationCodeParticipant" : "' + parameters.relationCode + '", ' +
                     '"ReservationNumber" : "' + parameters.reservationNumber + '", ' +
                     '"ReservationDetailNumber" : "' + parameters.reservationDetailNumber + '"' +
                '  }' +
                '}';

    return this.post(input);
  }

  setReservationStatus(parameters) : Promise<any> {
    var input = '{"action":"' +  BATCH.REST_RESERVATION_STATUS + '", ' +
                ' "params":{ ' +
                     '"Administration" : "' + parameters.administration.number + '", ' +
                     '"ReservationNumber" : "' + parameters.reservationNumber + '", ' +
                     '"StatusReservation" : "' + STATUS_ARRIVED + '"' +
                '  }' +
                '}';


    return this.post(input)
  }
}
