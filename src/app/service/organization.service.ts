import { Injectable } from '@angular/core';
import { Organization } from '../base/organization'
import { Activity, ActivityGroupObjectGroup } from '../base/activity'
import { BatchService } from './batch.service';
import { PlannerService } from './planner.service';
import { RelationService } from './relation.service';
import { ObjectGroup } from '../base/room';


@Injectable()
export class OrganizationService {
  activities : Activity[] = [];
  objectGroups = [];
  activityGroups = [];
  activityGroupObjectGroups : ActivityGroupObjectGroup[] = [];
  objects = [];

  organizations : Organization[] = [];

  constructor( private plannerService: PlannerService,
               private relationService: RelationService,
               private batchService: BatchService) { }

  setOrganization(data) {
    if (data) {
      //console.log(JSON.stringify(data))
      
      var _organizations = this.batchService.getRecordSet(data, ['code', 'name', 'order']);

      if (_organizations) {
        for (var o = 0; o < _organizations.length; o++) {
          var _organization = { code : '', name : '', order : 0,  persons : [], groups : [], roles : [], personGroups : [], personRoles : [], personActivities : [] };

          _organization.code = _organizations[o].code;
          _organization.name = _organizations[o].name;
          _organization.order = _organizations[o].order;

          this.organizations.push(_organization);
        }
      }

      var _persons = this.batchService.getRecordSet(data, ['code', 'name', 'job', 'codeRelation', 'codeOrganization']);

      if (_persons) {
      //voeg de personen toe aan de juiste organisatie
        for (var o = 0; o < this.organizations.length; o++) {
          for (var p = 0; p < _persons.length; p++) {
            var _record = _persons[p];

            if (_record.codeOrganization == this.organizations[o].code) {
              var _person1 = { code: '', name: '', job: '', relation: undefined };

              _person1.code = _record.code;
              _person1.name = _record.name;
              _person1.job = _record.job;

              //DONE : relatie koppelen!
              for (var r = 0; r < this.relationService.relations.length; r++) {
                if (_record.codeRelation == this.relationService.relations[r].code) {
                  _person1.relation = this.relationService.relations[r];
                }
              }

              this.organizations[o].persons.push(_person1);
            }
          }
        }
      }

      var _groups = this.batchService.getRecordSet(data, ['code', 'name', 'order', 'codeOrganization']);

      if (_groups) {
      //voeg de groepen toe aan de juiste organisatie
        for (var o = 0; o < this.organizations.length; o++) {
          for (var g = 0; g < _groups.length; g++) {
            var _record = _groups[g];

            if (_record.codeOrganization == this.organizations[o].code) {
              var _group = { code: '', name: '', order: 0 };

              _group.code = _record.code;
              _group.name = _record.name;
              _group.order = _record.order;

              this.organizations[o].groups.push(_group);
            }
          }
        }
      }

      var _roles = this.batchService.getRecordSet(data, ['name', 'codeOrganization']);

      if (_roles) {
        //voeg de rollen toe aan de juiste organisatie
        for (var o = 0; o < this.organizations.length; o++) {
          for (var r = 0; r < _roles.length; r++) {
            var _record =_roles[r];

            if (_record.codeOrganization == this.organizations[o].code) {
              var _role = { name: '' };

              _role.name = _record.name;

              this.organizations[o].roles.push(_role);
            }
          }
        }
      }

      var _groupUsers = this.batchService.getRecordSet(data, ['codeOrganization', 'codePerson', 'codeGroup']);

      if (_groupUsers) {
        //voeg de koppeling tussen de personen en groepen toe aan de juiste organisatie
        for (var o = 0; o < this.organizations.length; o++) {
          for (var pg = 0; pg < _groupUsers.length; pg++) {
            var _record = _groupUsers[pg];

            if (_record.codeOrganization == this.organizations[o].code) {
              var _person_group = { person: undefined, group: undefined, order: 0 };

              for (var p = 0; p < this.organizations[o].persons.length; p++) {
                var _person = this.organizations[o].persons[p];

                if (_person.code == _record.codePerson) {
                  _person_group.person = _person;
                }
              }

              for (var g = 0; g < this.organizations[o].groups.length; g++) {
                var _group = this.organizations[o].groups[g];

                if (_group.code == _record.codeGroup) {
                  _person_group.group = _group;
                }
              }

              this.organizations[o].personGroups.push(_person_group);
            }
          }
        }
      }

      var _userRoles = this.batchService.getRecordSet(data, ['codeOrganization', 'codePerson', 'roleName']);

      if (_userRoles) {
        //voeg de koppeling tussen de personen en rollen toe aan de juiste organisatie
        for (var o = 0; o < this.organizations.length; o++) {
          for (var pr = 0; pr < _userRoles.length; pr++) {
            var _record = _userRoles[pr];

            if (_record.codeOrganization == this.organizations[o].code) {
              var _person_role = { person: undefined, role: undefined };

              for (var p = 0; p < this.organizations[o].persons.length; p++) {
                var _person = this.organizations[o].persons[p];

                if (_person.code == _record.codePerson) {
                  _person_role.person = _person;
                }
              }

              for (var r = 0; r < this.organizations[o].roles.length; r++) {
                var _role = this.organizations[o].roles[r];

                if (_role.name == _record.roleName) {
                  _person_role.role = _role;
                }
              }

              this.organizations[o].personRoles.push(_person_role);
            }
          }
        }
      }

      var _activities = this.batchService.getRecordSet(data, ['groupCode', 'code', 'name', 'fontColor', 'backColor', 'type', 'minAttendees', 'maxAttendees']);

      if (_activities) {
        //voeg de activiteiten toe
        for (var a = 0; a < _activities.length; a++) {
          var _activity = _activities[a];

          this.activities.push(_activity);
        }
      }

      var _userActivities = this.batchService.getRecordSet(data, ['codeOrganization', 'codePerson', 'activityCode']);

      if (_userActivities) {
        //voeg de koppeling tussen de personen en activiteiten toe aan de juiste organisatie
        for (var o = 0; o < this.organizations.length; o++) {
          for (var pa = 0; pa < _userActivities.length; pa++) {
            var _record = _userActivities[pa];

            if (_record.codeOrganization == this.organizations[o].code) {
              var _person_activity = { person: undefined, activity: undefined };

              for (var p = 0; p < this.organizations[o].persons.length; p++) {
                var _person = this.organizations[o].persons[p];

                if (_person.code == _record.codePerson) {
                  _person_activity.person = _person;
                }
              }

              for (var a = 0; a < this.activities.length; a++) {
                var _activity1 = this.activities[a];

                if (_activity1.code == _record.activityCode) {
                  _person_activity.activity = _activity1;
                }
              }

              this.organizations[o].personActivities.push(_person_activity);
            }
          }
        }
      }

      this.activityGroups = this.batchService.getRecordSet(data, ['code', 'name']);
      this.objectGroups = this.batchService.getRecordSet(data, ['name', 'accommodationCode']);
      this.activityGroupObjectGroups = this.batchService.getRecordSet(data, ['activityGroup', 'objectGroup']);
      this.objects = this.batchService.getRecordSet(data, ['code', 'name', 'number', 'member', 'groupName']);


      //console.log('ACTIVITEITEN : ' + JSON.stringify(this.activities))

      //console.log('ORGANISATIE : ' + JSON.stringify(this.organizations))
    }
  }

  getActivitiesByObjectGroups(groups) : Activity[] {
    var _result = [];

    for (var a = 0; a < this.activities.length; a++) {
      var _found = false
      
      for (var a1 = 0; a1 < _result.length; a1++) {
        if (this.activities[a].groupCode == _result[a1].groupCode && this.activities[a].code == _result[a1].code) {
          _found = true;
        }
      }

      if (!_found) {
        var _activity = this.activities[a];
        var _foundGroups = 0;

        for (var ao = 0; ao < this.activityGroupObjectGroups.length; ao++) {
          var _ao = this.activityGroupObjectGroups[ao];
        
          if (_ao.activityGroup == _activity.groupCode) {
            if (groups.indexOf(_ao.objectGroup) >= 0) {
              _foundGroups++;
            }
          }
        }

        if (_foundGroups == groups.length) {
          _result.push(this.activities[a]);
        }
      }
    }

    return _result;
  }
}
