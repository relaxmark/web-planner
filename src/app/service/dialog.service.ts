import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


import { RlxDialog } from '../rlx-dialog';

@Injectable()
export class DialogService {

  constructor(
    public dialog: MatDialog, ) { }

  showMessage(message : string, caption : string) : any {
    let dialogRef = this.dialog.open(RlxDialog, { width: '480px', data: { caption: caption, message: message, buttons : [{caption : 'OK'}] }});

    return dialogRef;
  }

  confirm(question : string, caption : string) : any {
    let dialogRef = this.dialog.open(RlxDialog, { width: '480px', data: { caption: caption, message: question, buttons : [{caption : 'Ja'}, {caption : 'Nee'}] }});

    return dialogRef;
  }


  optionBox(message : string, caption : string, options : any[], parentObject : any, onClick : any) : any {
    let dialogRef = this.dialog.open(RlxDialog, { width: '480px', data: { caption: caption, message: message, buttons : [{caption : 'Sluiten', isCancel : true}], options: options, parentObject : parentObject, onClick : onClick }});

    return dialogRef;
  }
}
