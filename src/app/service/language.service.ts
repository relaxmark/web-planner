import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Injectable()
export class LanguageService {
  private currentLanguage = 'NL';

  constructor() { }

  setLanguage(code) {
    this.currentLanguage = code;
  }

  translate(original) : string {
    var _result = original;

    return _result;
  }
}
