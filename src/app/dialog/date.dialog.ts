import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ViewEncapsulation } from '@angular/core';

import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDatepickerInputEvent} from '@angular/material/datepicker';
import { MY_FORMATS } from '../const';

@Component({
  selector: 'date-dialog',
  templateUrl: 'date.dialog.html',
  styleUrls: [ './date.dialog.css' ],
  encapsulation: ViewEncapsulation.None,
  providers: [
      // The locale would typically be provided on the root module of your application. We do it at
      // the component level here, due to limitations of our example generation script.
      {provide: MAT_DATE_LOCALE, useValue: 'nl-NL'},

      // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
      // `MatMomentDateModule` in your applications root module. We provide it at the component level
      // here, due to limitations of our example generation script.
      {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
      {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    ],

})

export class DateDialog {
  caption = '';
  options = [];
  showCalendar = false;

  constructor(
    public dialogRef: MatDialogRef<DateDialog>,

    @Inject(MAT_DIALOG_DATA) public data: any) {
      if (data.caption) {
        this.caption = data.caption
      }
      else {
        this.caption = 'Kies een dag'
      }

      this.options.push({caption : 'Vandaag', date : new Date()})
      this.options.push({caption : 'Morgen', date : new Date(new Date().getTime() + 24 * 60 * 60 * 1000)}) ;
      this.options.push({date : new Date(new Date().getTime() + 2 * 24 * 60 * 60 * 1000)}) ;
      this.options.push({date : new Date(new Date().getTime() + 3 * 24 * 60 * 60 * 1000)}) ;
      this.options.push({date : new Date(new Date().getTime() + 4 * 24 * 60 * 60 * 1000)}) ;
      this.options.push({date : new Date(new Date().getTime() + 5 * 24 * 60 * 60 * 1000)}) ;
      this.options.push({date : new Date(new Date().getTime() + 6 * 24 * 60 * 60 * 1000)}) ;
      this.options.push({caption : 'Volg. week', date : new Date(new Date().getTime() + 7 * 24 * 60 * 60 * 1000)}) ;
      this.options.push({caption : 'Kalender...', special : true})
    }

  cancel() {
    this.dialogRef.close();
  }

  onOption(option) {
    if (option) {
      if (option.date) {
        this.dialogRef.close(option);
      }
      else {
        this.showCalendar = true;
      }
    }
  }

  calendarEvent(type : string, event : MatDatepickerInputEvent<Date>) {
    if (type == 'change') {
      this.options[this.options.length - 1].date = new Date(event.value);

      this.onOption(this.options[this.options.length - 1]);
    }
  }
}
