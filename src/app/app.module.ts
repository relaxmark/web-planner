import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MomentModule } from 'angular2-moment';

import { NgIdleModule} from '@ng-idle/core';

import { RouterGuard } from './guards/router.guard'

import { AppComponent }             from './app.component';
import { AdministrationComponent }  from './administration.component';
import { DashboardComponent }       from './dashboard.component';
import { PlannerComponent }         from './planner.component';
import { BatchService }             from './service/batch.service';
import { PlannerService }           from './service/planner.service';
import { OrganizationService }      from './service/organization.service';
import { ParamService }             from './service/param.service';
import { RelationService }          from './service/relation.service';
import { DialogService }            from './service/dialog.service';
import { AdministrationService }    from './service/administration.service';
import { DateService}               from './service/date.service';
import { DateDialog}                from './dialog/date.dialog';
import { RlxDialog }                from './rlx-dialog';

import { DialogToevoegen }          from './dialog-toevoegen';
import { DialogWijzigen }           from './dialog-wijzigen';
import { DialogVerplaatsen }        from './dialog-verplaatsen';

import { routing }                  from './app.routing';

import { MatDatepickerModule}      from '@angular/material/datepicker';
import { MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { MatSnackBarModule}        from '@angular/material';
import { MatAutocompleteModule}    from '@angular/material/autocomplete';
import { MatFormFieldModule }      from '@angular/material';
import { MatInputModule }          from '@angular/material';
import { MatSelectModule }         from '@angular/material/select';
import { MatSlideToggleModule }    from '@angular/material/slide-toggle';
import { MatSliderModule }         from '@angular/material/slider'


import { CookieModule } from 'ngx-cookie';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    routing,
    MomentModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSliderModule,
    NgIdleModule.forRoot(),
    CookieModule.forRoot()
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    PlannerComponent,
    AdministrationComponent,
    DialogToevoegen,
    DialogWijzigen,
    DialogVerplaatsen,
    DateDialog,
    RlxDialog,
  ],
  providers: [ BatchService, PlannerService, DateService, OrganizationService, ParamService, RelationService, DialogService, AdministrationService, RouterGuard ],
  bootstrap: [ AppComponent ],
  entryComponents: [DialogToevoegen, DialogWijzigen, DialogVerplaatsen, RlxDialog, DateDialog,]
})
export class AppModule { }
