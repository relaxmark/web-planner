import { Component, Directive, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';

import { PlannerService } from './service/planner.service';
import { ParamService } from './service/param.service';
import { OrganizationService } from './service/organization.service';
import { RelationService } from './service/relation.service';
import { AdministrationService } from './service/administration.service';


import { User } from './base/relation';
import { Administration } from './base/administration';
import { Accommodation } from './base/accommodation';

import { CookieService } from 'ngx-cookie';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  })

export class AppComponent implements OnInit, OnDestroy {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private plannerService: PlannerService,
    private paramService: ParamService,
    private organizationService : OrganizationService,
    private relationService : RelationService,
    private administrationService : AdministrationService,
    private idle: Idle,
    private cookieService : CookieService) {

      //console.log('AppComponent.Create');

      idle.setIdle(5);
      idle.setTimeout(600);
      idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

      idle.onTimeout.subscribe(() => {
      //geen automatisch uitloggen voor nu...
        //this.plannerService.user.code = 0;

        //if (this.plannerService.mode == 'browse') {
          //this.plannerService.timedOut = true;
        //}
      });

      this.reset();

      var adm : any = this.cookieService.getObject('adm');

      //console.log(cookie - adm = ' + JSON.stringify(adm));

      if (adm) {
        this.selectAdministration(adm);
      }
    }

  timedOut() : boolean {
    return this.plannerService.timedOut;
  }


  reset() {
    this.idle.watch();
    this.plannerService.timedOut = false;
  }

  title() : string {
    var _adm = this.administrationService.selectedAdministration;

    if (_adm) {
      return _adm.name;
    }
    else {
      return "GEEN ADMINISTRATIE GEVONDEN";
    }
  }

  administrations() : Administration[] {
    return this.administrationService.administrations;
  }

  administration() : Administration {
    return this.administrationService.selectedAdministration;
  }

  selectAdministration(adm : Administration) : void {
    this.administrationService.selectedAdministration = adm;
  }

  setAccommodations(data) {
    if (data) {
      this.plannerService.accommodations = data.recordset;
    }
    else {
      this.selectAdministration(this.administrationService.selectedAdministration);
    }
  }

  relation() : User {
    return this.plannerService.user;
  }

  accommodations() : Accommodation[] {
    return this.plannerService.accommodations;
  };

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }
}
