import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDatepickerInputEvent} from '@angular/material/datepicker';

import { PlannerService } from './service/planner.service';
import { AdministrationService } from './service/administration.service';

import { MY_FORMATS } from './const';

@Component({
  selector: 'dialog-wijzigen',
  templateUrl: 'dialog-wijzigen.html',
  styleUrls: [ './dialog-wijzigen.css' ],
  providers: [
      // The locale would typically be provided on the root module of your application. We do it at
      // the component level here, due to limitations of our example generation script.
      {provide: MAT_DATE_LOCALE, useValue: 'nl-NL'},

      // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
      // `MatMomentDateModule` in your applications root module. We provide it at the component level
      // here, due to limitations of our example generation script.
      {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
      {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    ],
})
export class DialogWijzigen {

  mode : string = 'menu';
  selectedDate = new Date();



  constructor(
    private plannerService: PlannerService,
    private administrationService: AdministrationService,
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<DialogWijzigen>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  delete() : void {
    alert('hier moet een vraag komen of je door wilt gaan...');

    this.plannerService.deleteBooking({administration: this.administrationService.selectedAdministration, accommodation: this.data.planner.accommodation, item: this.data.item}).then(data => { this.afterDelete(data) });
  }

  afterDelete(data : any) : void {
    var result = false;
    var message = '';

    if (data.recordsets.length) {
      if (data.recordsets[data.recordsets.length - 1].length) {
        result = data.recordsets[data.recordsets.length - 1][0].result;
        message = data.recordsets[data.recordsets.length - 1][0].additionalInfo;
      }
    }

    if (result) {
      this.dialogRef.close();
    }
    else {
      this.snackBar.open('Verwijderen is niet gelukt (' + message + ')', '', {duration: 5000})
    }
  }

  edit() : void {
    this.mode = 'edit';
  }

  goSelectDate() : void {
    this.mode = 'date';
  }

  goSelectTime() : void {
    this.dialogRef.close();
  }

  selectDate(event: MatDatepickerInputEvent<Date>) {
    //this.selectedDate = new Date(event.value);
    this.plannerService.selectedDate = new Date(event.value);

    this.data.planner.getRooms(this.data.planner.accommodation, false);

    this.dialogRef.close();
  }

  selectedBooking() : string {
    return this.data.item.caption;
  }

  createOnDate() : void {
    var _parameters = { accommodation: this.data.planner.accommodation,
                        date: this.selectedDate,
                        itemStart: this.data.item,
                        itemEnd: this.data.item
                      }

    this.plannerService.addBooking(_parameters)
      .then(result => { if (result) { this.dialogRef.close() } });
  }

  cancel() : void {
    if (this.mode == 'edit') {
      this.mode = 'menu'
    }
    else if (this.mode == 'menu') {
      this.dialogRef.close();
    }
  }

  save() : void {
    alert('we moeten de wijzigingen gaan opslaan');
  }
}
