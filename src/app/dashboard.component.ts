import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { PlannerService } from './service/planner.service';
import { AdministrationService } from './service/administration.service';
import { BatchService, BATCH } from './service/batch.service';
import { ParamService } from './service/param.service';
import { DialogService } from './service/dialog.service';

import { User } from './base/relation';

import { CookieService } from 'ngx-cookie';

@Component({
  selector: 'my-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})

export class DashboardComponent implements OnInit {
  activationCode = '';
  resetPasswordCode = '';
  mode = 'login';
  newUser = {email : '', password: '', passwordCheck: '', firstName: '', familyName : ''}
  message = '';
  errorCode = 0;

  constructor(
    private plannerService: PlannerService,
    private administrationService: AdministrationService,
    private paramService: ParamService,
    private dialogService: DialogService,
    private cookieService : CookieService,
    private route: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
    private batchService: BatchService,) {
      //console.log('DashboardComponent.Create');
      
      this.route.params.subscribe( params => {
        //console.log('ROUTE : ' + this.router.url)

        if (this.router.url.includes('/resetpassword/')) {
          this.resetPasswordCode = params.id;
        }
        else {
          this.activationCode = params.id;
        }


        if (this.activationCode || this.resetPasswordCode) {
          this.plannerService.getAccount({ resendActivation : this.activationCode, resetPassword : this.resetPasswordCode })
            .then(result => {
              //console.log('INQUIRY - ' + JSON.stringify(result))

              var _recordset = this.batchService.getRecordSet(result, ['administrationNumber', 'fullName', 'emailAddress']);

              if (_recordset) {
                this.administrationService.selectAdministration(this.administrationService.administrationByNumber(_recordset[0].administrationNumber));
                this.user().email = _recordset[0].emailAddress;
                this.user().password = '';
              }

              //console.log('AFTER INQUIRY - ' + JSON.stringify(this.user()))

              //console.log(JSON.stringify(this.administrationService.selectedAdministration))
            })

          if (this.activationCode) {
            this.mode = 'activate';
          }

          if (this.resetPasswordCode) {
            this.mode = 'resetPassword';
          }
        }
      })


      //this.dialogService.showMessage('Dit is een simpele message-box', 'planner');

      //this.dialogService.showMessage('Het kan ook ingewikkelder, met hele lange regels die dan netjes afgekapt worden\r\n\r\nOf met tekst\r\nover meerdere\r\nregels\r\nwat uiteindelijk\r\nresulteer in\r\neenscrollbar', 'planner');

      //var dialog = this.dialogService.confirm('Mark snapt het?', 'Planner');

      //dialog.afterClosed().subscribe(result => {
        //if (result && result.caption == 'Ja') {
          //alert('hij snapt het')
        //} else
        //{
          //alert('hij heeft geen idee');
        //}
      //});

      //var dialog = this.dialogService.optionBox('Waar heeft u vandaag zin in?\r\n\r\nMaak uw keuze!', 'Optionbox', [{caption : 'Friet van Piet'}, {caption : 'Fred kroket'}, {caption : '1-pans Hans'}, {caption : 'Broodje van Cootje'}], this.onClickOption);

      //dialog.afterClosed().subscribe(result => {
        //alert('ok...we zijn klaar')
      //})
    }

  onClickOption(option) {
    alert(option.caption);
  }

  login(): void {
    var temp : User;

    this.errorCode = 0;

    this.plannerService.doLogin(this.administrationService.selectedAdministration, this.user(), this.activationCode).then(result => {
      //console.log(' login result = ' + JSON.stringify(result));


      var _batch = this.batchService.evaluateBatch(result, BATCH.REST_LOGIN);

      this.message = _batch.message;
      this.errorCode = _batch.errorCode;

      if ( _batch.success ) {
        var _users = this.batchService.getRecordSet(result, ['relationName', 'relationEmail', 'relationCode', 'roleName']);
        var _rights = this.batchService.getRecordSet(result, ['type', 'name', 'index', 'assigned']);

        if (_users) {
          this.setUser(_users[0]);
        }
        else {
          console.error('DashboardComponent - gebruiker gegevens niet gevonden')
        }

        if (_rights) {
          this.setUserRights(_rights);
        }
        else {
          console.error('DashboardComponent - rechten gegevens niet gevonden')
        }

        this.plannerService.getParams(this.administrationService.selectedAdministration, this.plannerService.user).then(result => { this.paramService.setData(result) } );

        this.cookieService.putObject('user', this.user);

        if (this.plannerService.accommodations && this.plannerService.accommodations.length) {
          this.router.navigate(['/planner', this.plannerService.accommodations[0].nameShort])
        }
      }
    });
  }

  goBack() {
    this.mode = 'login';
  }

  test() : void {
    //console.log('TEST - ' + JSON.stringify(this.user()));
  }

  logout(): void {
    this.plannerService.user.code = 0;

    this.cookieService.removeAll();

  }

  user() : any {
    //console.log('PLANNERSERVICE : ' + JSON.stringify(this.plannerService.user));

    return this.plannerService.user;
  }

  lostPassword() {
    this.mode = 'password'
  }

  setUser(user : any) : void {
    this.plannerService.user.code = user.relationCode;
    this.plannerService.user.name = user.relationName;
    this.plannerService.user.roleName = user.roleName;
  }

  setUserRights(rights : any) : void {
    this.plannerService.user.rights = [];

    if (rights) {
      for (var r = 0; r < rights.length; r++) {
        this.plannerService.user.rights.push(rights[r])
      }
    }
  }

  administration() : string {
    var _adm = this.administrationService.selectedAdministration;

    if (_adm) {
      return _adm.name;
    }
    else {
      return 'GEEN ADMINISTRATIE';
    }
  }

  caption() : string {
    var _mode = 'inloggen';

    if (this.mode =='new') {
      _mode = 'registreren';
    }

    if (this.mode =='password') {
      _mode = 'Wachtwoord vergeten';
    }

    if (this.mode == 'activate') {
      _mode = 'Account activeren';
    }

    if (this.mode =='resetPassword') {
      _mode =  'Wachtwoord instellen'
    }

    return this.administration() + ' - ' + _mode;
  }

  ngOnInit(): void {
  }

  goNewUser() {
    this.errorCode = 0;
    this.mode = 'new'
  }

  addNewUser(user) {
    //console.log(JSON.stringify(user))

    this.plannerService.addAccount({ administration : this.administrationService.selectedAdministration, user : user }).then(result => {

      var _success = false;
      var _reason = '<onbekende reden>'

      console.log('REGISTREREN --> ' + JSON.stringify(result))

      if (result) {
        if (result.recordsets.length) {
          if (result.recordsets[0].length) {
            if (result.recordsets[0][0].failure) {

              _reason = result.recordsets[0][0].reason
            }
            else {
              _success = true;
            }
          }
        }
        else {
          _reason = 'registratie-verzoek geeft niet aan of het gelukt is'
        }
      }
      else {
        _reason = 'registratie-verzoek geeft geen antwoord'
      }

      if (_success) {
        var dialog = this.dialogService.showMessage('Uw gegevens zijn succesvol geregistreerd\n\nEr is een E-mail verzonden met een activatielink.\n\nU dient het account te activeren om te kunnen inloggen', 'Registratie succesvol' )

        dialog.afterClosed().subscribe(result => {
          this.mode = 'login'

          this.user().email = this.newUser.email;
          this.user().password = '';
        })
      }
      else {
        this.dialogService.showMessage('Uw account is niet aangemaakt!\n\n(' + _reason + ')', 'Nieuwe gebruiker')
      }
    })
  }

  resendActivation() {
    this.plannerService.resendActivation( {administration : this.administrationService.selectedAdministration, email : this.plannerService.user.email} )
      .then(result => {
        console.log('RESEND ACTIVATION : ' + JSON.stringify(result))
      })
  }

  buttonCaption() {
    var _result = "Inloggen"

    if (this.mode == 'activate') {
      _result = 'Activeren'
    }
    if (this.mode == 'resetPassword') {
      _result = 'Reset'
    }

    return _result;
  }

  sendLostPassword(email) {
    var _parameters = { administration : this.administrationService.selectedAdministration, 
                        email : this.user().email 
                      }

    this.plannerService.sendLostPassword(_parameters)
      .then(result => {
        console.log('RESET PASSWORD : ' + JSON.stringify(result))
      })
              

  }

  resetPassword() {
    if (this.user().password == this.user().passwordConfirm) {
      var _parameters = { administration : this.administrationService.selectedAdministration, 
                          email : this.user().email,
                          password : this.user().password
                        }

      this.plannerService.resetPassword(_parameters)
        .then(result => {
          console.log('RESET PASSWORD : ' + JSON.stringify(result))
        })

    }
    else {
      this.dialogService.showMessage('De wachtwoorden zijn niet gelijk aan elkaar', 'Wachtwoord instellen')
    }
  }

  isValidPassword(password) : boolean {
    if (password) {
      if (password != '' && password.length < 8) {
        return false
      }
    }

    return true
  }

  isSamePassword(password1, password2) : boolean {
    if (password1) {
      if (password1 != password2) {
        return false
      }
    }

    return true
  }
}
