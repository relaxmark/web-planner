import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { PlannerService } from './service/planner.service';
import { AdministrationService } from './service/administration.service';
import { OrganizationService } from './service/organization.service';
import { RelationService } from './service/relation.service';
import { ParamService } from './service/param.service';

import { Administration } from './base/administration';

import { CookieService } from 'ngx-cookie';


@Component({
  templateUrl: './administration.component.html',
  styleUrls: [ './administration.component.css' ]
})

export class AdministrationComponent implements OnInit {
  constructor(
    private plannerService: PlannerService,
    private organizationService: OrganizationService,
    private relationService: RelationService,
    private paramService: ParamService,
    private administrationService : AdministrationService,
    private router: Router,
    private cookieService: CookieService) {
      //console.log('AdministrationComponent.Create')
    }

  ngOnInit(): void {
    
  }

  administration() {
    return this.administrationService.selectedAdministration;
  }

  administrations() {
    return this.administrationService.administrations;
  }

  selectAdministration(adm : Administration) {
    this.administrationService.selectAdministration(adm)
    this.router.navigate(['/dashboard'])
  }

}
