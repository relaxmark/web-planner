USE [SportPlanner]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [com].[Batch];
DROP TABLE [com].[BatchParameter];
GO

DROP PROCEDURE uspBatch_Create;
DROP PROCEDURE [btc].[getUser];
DROP PROCEDURE [btc].[getAccommodations];
DROP PROCEDURE [btc].[getObjects];
DROP PROCEDURE [btc].[getAdministrations];
DROP PROCEDURE [btc].[getReservations];
DROP PROCEDURE [btc].[getRelations];
GO

CREATE TABLE [com].[Batch](
	[GUID] [uniqueidentifier] NOT NULL,
	[Action] [int] NULL,
	[Input] [varchar](4096) NULL,
	[Status] [smallint] NULL,
	[TS_Creation] [datetime] NOT NULL,
	[TS_Modified] [datetime] NOT NULL,
	[GUID_UserWhoCreated] [uniqueidentifier] NOT NULL,
	[GUID_UserWhoModified] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Batch] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [com].[Batch] ADD  DEFAULT (getdate()) FOR [TS_Creation]
GO

ALTER TABLE [com].[Batch] ADD  DEFAULT (getdate()) FOR [TS_Modified]
GO

ALTER TABLE [com].[Batch] ADD  DEFAULT ('BDBF947B-CBC5-40E1-8EB6-D0148F1D9FA1') FOR [GUID_UserWhoCreated]
GO

ALTER TABLE [com].[Batch] ADD  DEFAULT ('BDBF947B-CBC5-40E1-8EB6-D0148F1D9FA1') FOR [GUID_UserWhoModified]
GO


CREATE TABLE [com].[BatchParameter](
	[GUID] [uniqueidentifier] NOT NULL,
	[GUID_Batch] [uniqueidentifier] NOT NULL,
	[InputOutput] [bit] NOT NULL,
	[ParameterName] [varchar](256) NOT NULL,
	[ParameterValue] [varchar](1024) NULL,
	[ParameterType] [int] NULL,
	[TS_Creation] [datetime] NOT NULL,
	[TS_Modified] [datetime] NOT NULL,
	[GUID_UserWhoCreated] [uniqueidentifier] NOT NULL,
	[GUID_UserWhoModified] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_BatchParameter] PRIMARY KEY CLUSTERED 
(
	[GUID_Batch] ASC,
	[InputOutput] ASC,
	[ParameterName] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UK_BatchParameter] UNIQUE NONCLUSTERED 
(
	[GUID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [com].[BatchParameter] ADD  DEFAULT (getdate()) FOR [TS_Creation]
GO

ALTER TABLE [com].[BatchParameter] ADD  DEFAULT (getdate()) FOR [TS_Modified]
GO

ALTER TABLE [com].[BatchParameter] ADD  DEFAULT ('BDBF947B-CBC5-40E1-8EB6-D0148F1D9FA1') FOR [GUID_UserWhoCreated]
GO

ALTER TABLE [com].[BatchParameter] ADD  DEFAULT ('BDBF947B-CBC5-40E1-8EB6-D0148F1D9FA1') FOR [GUID_UserWhoModified]
GO


-- Batchjob 1:	 Retrieve UserData based on an email-address and a password
CREATE PROCEDURE [btc].[getUser]
	@guid_batch uniqueidentifier
AS
BEGIN
	DECLARE @email VARCHAR(50) = (select cast(ParameterValue as VARCHAR(50)) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Email');
	DECLARE @password VARCHAR(20) = (select cast(ParameterValue as VARCHAR(20)) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Password');

	DECLARE @guid VARCHAR(50) = (select GUID from rel.Relation where PrimaryEmailAddress = @Email);

	-- The NOCOUNT prevents the output from display the messages 
	set NOCOUNT ON;
	SELECT rel.FirstName + ' ' + ' ' + rel.LastName as name, 
	       rel.PrimaryEmailAddress as email, rel.Password as password, 
		   rel.RelationCode as code,
           r.RoleName as roleName,
           cast(COALESCE(r.ShowReservations, 0) as BIT) as allowShow,
           cast(COALESCE(r.CreateReservation, 0) as BIT) as allowCreate,
           cast(COALESCE(r.DeleteReservation, 0) as BIT) as allowDelete,
           cast(COALESCE(r.SelectMultipleTimes, 0) as BIT) as allowMultiTime,
           cast(COALESCE(r.SelectMultipleRooms, 0) as BIT) as allowMultiRoom,
           cast(COALESCE(r.CreateReservationOnBehalf, 0) as BIT) as allowCreateOnBehalf
    FROM rel.Relation rel
      left join [com].[User] u on u.guid_relation = rel.guid
      left join [com].[UserRole] ur on ur.guid_user = u.guid
      left join [com].[Role] r on r.guid = ur.guid_role
	WHERE rel.GUID     = @guid 
	  and rel.Password = @password 
END

GO

--2
CREATE PROCEDURE [btc].[getAccommodations]
	@guid_batch uniqueidentifier
AS
BEGIN
	DECLARE @adm INT = (select cast(ParameterValue as INT) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Administration');

	-- The NOCOUNT prevents the output from only outputting the result of the select and not the SQL messages 
    set NOCOUNT ON;

	SELECT acc.FullName as nameFull, 
		   acc.ShortName as nameShort, 
		   p.TimeLineInterval as timeLineInterval, 
		   p.BookingInterval as bookingInterval,
		   cast(COALESCE(p.ShowTimeLineLeftSide, 1) as BIT) as showTimeLineLeft,
		   cast(COALESCE(p.ShowTimeLineRightSide, 0) as BIT) as showTimeLineRight,
		   cast(COALESCE(p.ShowTimeLinePerShift, 1) as BIT) as showTimeLineShift,
		   cast(COALESCE(p.ShowTimesinCells, 0) as BIT) as showTimeInCell
    FROM pln.Accommodation acc
    inner join com.Administration adm on adm.guid = acc.GUID_Administration
	  left join pln.Planner p on acc.GUID = p.GUID_Accommodation
   WHERE adm.AdministrationNumber = @adm
   ORDER BY intOrder
END

GO


--Batchjob 3: Retrieve a list of objects, time-intervals, bookings and object-groupings
CREATE PROCEDURE [btc].[getObjects]
	@guid_batch uniqueidentifier
AS
BEGIN
	DECLARE @administration INT = (select cast(ParameterValue as INT) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Administration');
	DECLARE @accommodation VARCHAR(20) = (select cast(ParameterValue as VARCHAR(50)) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Accommodation');
	DECLARE @plandate VARCHAR(20) = (select cast(ParameterValue as VARCHAR(20)) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Date');
	
  -- The NOCOUNT prevents the output from only outputting the result of the select and not the SQL messages 
  set NOCOUNT ON;
	
  select EL.EnumerationIndex as enumIndex, cast(@plandate + ' ' + cast(ATTI.StartTime as varchar(5)) as datetime) as startTime, 
         cast(@plandate + ' ' + cast(ATTI.EndTime as varchar(5)) as datetime) as endTime, ATTI.TimeLineInterval as timeLineinterval, 
		 ATTI.BookingInterval as bookingInterval, 
         O.ObjectNumber as number, O.ShortName as name, O.Member as member
   from pln.Accommodation A 
     inner join pln.AccommodationTemplate AT on AT.GUID_Accommodation = A.GUID
     inner join pln.AccommodationCalendar AC on AC.GUID_AccommodationTemplate= AT.GUID
	 inner join com.Enumeration E on E.EnumerationName = 'TimeInterval'
	 inner join com.EnumerationList EL on EL.GUID_Enumeration = E.GUID 
	 inner join pln.AccommodationTemplateTimeInterval ATTI on ATTI.GUID_AccommodationTemplate = AT.GUID
     inner join pln.ObjectGrouping OG on OG.GUID = ATTI.GUID_ObjectGrouping 
     inner join pln.ObjectGroupingObject OGO on OGO.GUID_ObjectGrouping = OG.GUID
     inner join pln.Object O on O.GUID = OGO.GUID_Object
   where AC.CalendarDate = @plandate
     and A.ShortName = @accommodation
	 and ATTI.EnumerationIndex = EL.EnumerationIndex 
   order by number, enumIndex, startTime;

   select distinct O.ObjectNumber as room, PD.PlannerDate as date, NumberOfReservations as quantity
     from pln.PlannerDetail PD
       inner join pln.Planner P on P.GUID = PD.GUID_Planner
       inner join pln.Accommodation A on A.GUID = P.GUID_Accommodation
       inner join pln.Object O on O.GUID = PD.GUID_Object
    where A.ShortName = @accommodation
      and CONVERT(date, PD.PlannerDate) = @plandate;

  select [DATE] + cast(Starting as datetime) as timeStart,
         [DATE] + cast(Ending as datetime) as timeEnd,
         [Object] as room, RelationCode as relation,
         [ReservationNumber] as reservation,
         [Firstname] + ' ' + [LastName] as relationName
    from pln.PlannerReservations
  where ADM = @administration
    and ACC = @accommodation
    and DATE = @plandate

  select distinct O.ObjectNumber as number, O.ShortName as name, OG.GroupName as groupName, O.Member as member
    from pln.Object O
	  inner join pln.ObjectGroupingObject OGO on OGO.GUID_Object = O.GUID
	  inner join pln.ObjectGrouping OG on OG.GUID = OGO.GUID_ObjectGrouping
	  inner join pln.Accommodation A on A.GUID = OG.GUID_Accommodation
	  inner join pln.AccommodationTemplate ACT on ACT.GUID_Accommodation = A.GUID
	  inner join pln.AccommodationTemplateTimeInterval ATTI on ATTI.GUID_AccommodationTemplate = ACT.GUID and ATTI.GUID_ObjectGrouping = OG.GUID
	  inner join pln.AccommodationCalendar AC on AC.GUID_AccommodationTemplate = ACT.GUID
   where AC.CalendarDate = @plandate
     and  A.ShortName = @accommodation 
    order by number
END

-- Bacthjob 4: retrieve available administrations
CREATE PROCEDURE [btc].[getAdministrations]
	@guid_batch uniqueidentifier
AS
BEGIN
  set nocount on;

  select AdministrationNumber as number, AdministrationName as name
    from com.Administration
   order by name
END

GO


-- Batchjob 5 / 6: Add / Update / Delete booking
CREATE PROCEDURE [btc].[getReservations]
  @guid_batch uniqueidentifier
AS
BEGIN
  DECLARE @action VARCHAR(10) = (select cast(ParameterValue as VARCHAR(10)) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Action');
  DECLARE @adm INT = (select cast(ParameterValue as INT) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Administration');
  DECLARE @accCode VARCHAR(10) = (select cast(ParameterValue as VARCHAR(10)) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Accommodation');
  DECLARE @object INT = (select cast(ParameterValue as INT) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Room');
  DECLARE @relation INT = (select cast(ParameterValue as INT) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Relation');
  DECLARE @reservation VARCHAR(20) = (select cast(ParameterValue as VARCHAR(20)) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Reservation');

--  select @reservation as reservationNumber;

  DECLARE @GUID_Administration uniqueidentifier = (select GUID from com.Administration A where A.AdministrationNumber = @adm)
  DECLARE @GUID_Accommodation uniqueidentifier = (select GUID from pln.Accommodation A where A.GUID_Administration = @GUID_Administration and A.ShortName = @accCode)
  DECLARE @GUID_Planner uniqueidentifier = (select P.GUID from pln.Planner P inner join pln.Accommodation A on A.GUID = P.GUID_Accommodation where A.ShortName = @accCode);
  DECLARE @GUID_Object uniqueidentifier = (select O.GUID from pln.Object O where O.GUID_Accommodation = @GUID_Accommodation and O.ObjectNumber = @object);
  DECLARE @GUID_Reservation uniqueidentifier = (select res.GUID from res.Reservation res where res.GUID_Administration = @GUID_Administration and res.ReservationNumber = @reservation);

--  select @GUID_Reservation as GUID_Reservation;

  DECLARE @datum VARCHAR(10) = (select cast(ParameterValue as VARCHAR(10)) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Date');
  DECLARE @starttijd VARCHAR(5) = (select cast(ParameterValue as VARCHAR(5)) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'StartTime');
  DECLARE @eindtijd VARCHAR(5) = (select cast(ParameterValue as VARCHAR(5)) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'EndTime');


  DECLARE @START datetime = @datum + ' ' + @starttijd;
  DECLARE @END datetime = @datum + ' ' + @eindtijd;
  DECLARE @GUID_Relation uniqueidentifier = (select R.guid from rel.Relation R where R.GUID_Administration = @GUID_Administration and R.RelationCode = @relation);

  EXEC pln.SP_EditPlanner @action, @GUID_Planner, @GUID_Object, @START, @END, @GUID_Relation, @GUID_Reservation;
END


GO

--7 
CREATE PROCEDURE [btc].[getRelations]
	@guid_batch uniqueidentifier
AS
BEGIN
  DECLARE @adm INT = (select cast(ParameterValue as INT) from [com].[BatchParameter] where GUID_Batch = @guid_batch and InputOutput = 1 and ParameterName = 'Administration');
  DECLARE @guid_adm uniqueidentifier = (select GUID from [com].[Administration] where AdministrationNumber = @adm);

  set nocount on;
  select RelationCode as code, FirstName + ' ' + LastName as name, Gender as isMale
    from [rel].[Relation]
   where GUID_Administration = @guid_adm
   order by name
END

GO


CREATE PROCEDURE [dbo].uspBatch_Create 
  @action INT,
  @parameters VARCHAR(4096)
AS
BEGIN
  DECLARE @GUID uniqueidentifier = newid();
 
  INSERT INTO [com].[Batch] (GUID, Action, Input, Handled, Status) 
    values (@GUID, @Action, @Parameters, 0,1)

  INSERT INTO [com].[BatchParameter] (GUID, GUID_Batch, Input, Name, Value, Type) 
	SELECT newid(), @GUID, 1, * from OPENJSON(@parameters)
	
  update [com].[Batch] set Status = 5 where GUID = @GUID;
END
GO

CREATE TRIGGER [com].[TR_AFTER_UPDATE]
   ON  [com].[Batch]
   AFTER UPDATE
AS 
BEGIN
  
  SET NOCOUNT ON;

  begin try

    declare @GUID uniqueidentifier;
	declare @StoredProcedure varchar(256);
	declare @SQL_Query nvarchar(max);
	declare @SQL_Params nvarchar(1024);

	select @GUID = I.GUID, @StoredProcedure = EL.EnumerationValue from inserted I
	inner join deleted D on D.GUID = I.GUID
	inner join com.Enumeration E on E.EnumerationName = 'BatchProcedure'
	inner join com.EnumerationList EL on EL.GUID_Enumeration = E.GUID and EL.EnumerationIndex = I.Action
	where I.Status = 5 and D.Status <> 5;
	
    set @SQL_Query = N'EXEC ' + ltrim(rtrim(@StoredProcedure)) + N' @GUID_Batch';
	set @SQL_Params = N'@GUID_Batch uniqueidentifier'; 

	exec sp_executesql @SQL_Query, @SQL_Params, @GUID_Batch = @GUID;

	update com.Batch set Status = 6 where GUID = @GUID;

	 insert into log.Batchlog
	   values(newid(), getdate(),  @StoredProcedure, N'Gelukt!')
	 

  end try
  begin catch


    exec [pln].[usp_ErrorLog] 
		@AdditionalInfo = 'De batchopdracht kan niet worden uitgevoerd';

  end catch;
END

GO

ALTER TABLE [com].[Batch] ENABLE TRIGGER [TR_AFTER_UPDATE]
GO

/* SP_AddPlanner, een entry aanmaken in de planner

  Parameters

  - GUID van de planner
  - Object  (bijv. baan 1)
  - Datum start
  - Datum Einde
  - GUID_Reservation

  Het tijdvak Datum Start tot Datum Einde wordt vertaald naar Datum Uur tijdvak
  en roept daarna sp_aaddPlannerDetail aan

	2017-10-31  19:00
	2017-11-01  02:30				- sluitingstijd PLanner = middernacht

  wordt:
  
	2017-10-31  19:00
	2017-10-31  19:45
	2017-10-31  20:30
	2017-10-31  21:15
	2017-10-31  22:00
	2017-10-31  22:45
	2017-10-31  23:30				<-- wordt weggegooid 
	2017-11-01  00:30				<-- wordt weggegooid 
    2017-11-01  01:15				<-- wordt weggegooid 
    2017-11-01  02:00				<-- wordt weggegooid 

  
*/

CREATE PROCEDURE [pln].[SP_EditPlanner]
    @Action				varchar(20),
	@GUID_Planner		uniqueidentifier,
	@GUID_Object        uniqueidentifier,
	@PlannerDateStart	datetime,
	@PlannerDateEnd   	datetime,
	@GUID_Relation		uniqueidentifier,
	@GUID_ReservationIn	uniqueidentifier
AS
BEGIN
    
	--Bepaal aan de hand van de meegestuurde GUID_Planner de GUID_Administration en de GUID_Accommodation
	DECLARE @GUID_Administration uniqueidentifier = (SELECT ADM.GUID FROM com.Administration ADM inner join pln.Planner P on P.GUID_Administration = ADM.GUID and P.GUID = @GUID_Planner);
	DECLARE @GUID_Accommodation  uniqueidentifier = (SELECT A.GUID   FROM pln.Accommodation A inner join pln.Planner P on P.GUID_Accommodation = A.GUID and P.GUID = @GUID_Planner);
	declare @GUID_ReservationOut uniqueidentifier;

	DECLARE @DurationMinutes     smallint = datediff(mi, @PlannerDateStart, @PlannerDateEnd);
	
	DECLARE @StartTimeBooking	 time = cast(@PlannerDateStart as time),
			@EndTimeBooking      time = cast(@PlannerDateEnd   as time); 

	DECLARE @StartTimeRange      time, @EndTimeRange   time, @TimeLineInterval time;
	DECLARE @Minute              smallint;

	DECLARE @Result              smallint;
	DECLARE @AdditionalInfo      varchar(max);

   -- Find the Starting time, the ending time and interval for the given object	
   -- EnumerationIndex 1 = Openingstimes, 2-(bookable) Block times, 3- Closed

   -- Drop de benodige temp-table als die (nog) bestaat
	IF OBJECT_ID('tempdb..#TimeRange') IS NOT NULL DROP TABLE #TimeRange;

	create table #TimeRange(
		StartTime time,
		EndTime time,
		EnumerationIndex smallint,
		TimeLineInterval time,
		BookingInterval time,
		Marked bit);
  
    --select @GUID_Administration, @GUID_Accommodation, @GUID_Object, @PlannerDateStart;

	WITH OpeningTimes AS (
		SELECT ATTI.StartTime, ATTI.EndTime, ATTI.EnumerationIndex, ATTI.TimeLineInterval, ATTI.BookingInterval
		FROM       [pln].[Accommodation] A
		inner join [pln].[AccommodationTemplate] AT					on AT.GUID_Accommodation = A.GUID
		inner join [pln].[AccommodationCalendar] AC					on AC.GUID_AccommodationTemplate = AT.GUID
		inner join [pln].[AccommodationTemplateTimeInterval] ATTI	on ATTI.GUID_AccommodationTemplate = AT.GUID
		inner join [com].[Enumeration] E                            on E.GUID = ATTI.GUID_Enumeration
		inner join [com].[EnumerationList] EL                       on EL.GUID_Enumeration = E.GUID
		inner join [pln].[ObjectGrouping] OG                        on OG.GUID = ATTI.Guid_ObjectGrouping
		inner join [pln].[ObjectGroupingObject] OGO                 on OGO.GUID_ObjectGrouping = OG.GUID
		inner join [pln].[Object] O                                 on O.GUID = OGO.GUID_Object
		WHERE  A.GUID    = @GUID_Accommodation
		  and CONVERT(date, AC.CalendarDate) = CONVERT(date, @PlannerDateStart)
		  and EL.EnumerationIndex < 3
		  and  O.GUID = @GUID_Object
	)
    INSERT INTO #TimeRange (StartTime,EndTime, EnumerationIndex, TimeLineInterval, BookingInterval)
    SELECT StartTime,EndTime, EnumerationIndex, TimeLineInterval, BookingInterval 
	  FROM OpeningTimes;
   
   --select * from #TimeRange;

   -- If the above query resolves in no records, try to find the default template
	if not exists(SELECT TOP 1 StartTime FROM #TimeRange)
	BEGIN
		WITH DefaultOpeningTimes AS (
			SELECT ATTI.StartTime, ATTI.EndTime, ATTI.EnumerationIndex, ATTI.TimeLineInterval, ATTI.BookingInterval
			FROM [pln].[Accommodation] A
			inner join [pln].[AccommodationTemplate] AT					on AT.GUID_Accommodation = A.GUID
			inner join [pln].[AccommodationTemplateTimeInterval] ATTI	on ATTI.GUID_AccommodationTemplate = AT.GUID
			WHERE  A.GUID     = @GUID_Accommodation
			  and AT.TemplateName = 'Standaard'
		)
		INSERT INTO #TimeRange (StartTime,EndTime, EnumerationIndex, TimeLineInterval, BookingInterval)
		SELECT StartTime,EndTime, EnumerationIndex, TimeLineInterval, BookingInterval 
		FROM DefaultOpeningTimes;
	END;


	SELECT @StartTimeRange   = StartTime,
           @EndTimeRange     = EndTime,
		   @TimeLineInterval = TimeLineInterval 
    FROM #TimeRange 
    WHERE DATEDIFF(SECOND, @StartTimeBooking , StartTime) <= 0
	  and DATEDIFF(SECOND, @EndTimeBooking, EndTime) >= 0 

    --select @TimeLineInterval;

   -- Is er nu een tijdvak waarbinnen de gewenste boeking kan worden gemaakt
	if @TimeLineInterval <> '00:00'
	BEGIN

     -- Gebruik onderstaande declaratie om het deel eronder te tesetn
	 --declare @TimeLineInterval time = '00:15';
     
	 --  Splits de tijdvakken in hun tijdsinterval
	 -- Drop de benodige temp-table als die (nog) bestaat
		IF OBJECT_ID('tempdb..#TimeInterval') IS NOT NULL DROP TABLE #TimeInterval;

		CREATE TABLE #TimeInterval(
			Number int,
			HourPart int,
			MinutePart int,
			StartTime time,
			EndTime time,
			Marked bit
		);

		-- strip het aantal minuten uit het interval, bijv. 15
		set @Minute = DATEPART(minute, @TimeLineInterval);

		WITH CTEGenerate(Number) AS (
			SELECT 0 AS Number
			UNION ALL
			SELECT Number + 1 FROM CTEGenerate where Number < (1440 / @Minute)
		)
     
		INSERT INTO #TimeInterval(Number) SELECT Number FROM CTEGenerate OPTION(maxrecursion 1440);

		UPDATE #TimeInterval
		SET StartTime  = CONVERT(time(0), DATEADD(MINUTE, Number * @Minute, '0:00'))
		WHERE Number < (1440 / @Minute);
     
	    DELETE FROM #TimeInterval
		WHERE StartTime  < @StartTimeBooking
		   or StartTime >= @EndTimeBooking
		   or StartTime IS NULL;

		UPDATE #TimeInterval
			SET HourPart   = DATEPART(HOUR, #TimeInterval.StartTime),
				MinutePart = (Number % (60 / @minute)) + 1;

		/*
		-- Na 1x draaien kan je de EXEC los testen door onderstaande variablen even te unmarken 
		-- en samen te draaien met de select eronder
		declare @aGuid_Planner		uniqueidentifier = 'B8B858D0-625C-41DC-87EC-D7AB4262EB15';      -- SquashPlanner
		declare @aGUID_Object		uniqueidentifier = '7D010F16-A15B-48B9-9838-8A2C85B4A289';		-- Baan 2 (leden)
		declare @aGuid_Reservation	uniqueidentifier = 'B8B858D0-625C-41DC-87EC-D7AB4262EB15';      -- Onzin GUID
		declare @aPlannerDateStart	datetime         = '2017-11-17 10:00:00';
		*/

		--select * from #TimeInterval;

		BEGIN TRANSACTION;

		DECLARE @hourPart INT;
		DECLARE @minutePart INT;
		DECLARE myCursor CURSOR FORWARD_ONLY FOR SELECT HourPart, MinutePart FROM #TimeInterval;
	     
		begin try

		  OPEN myCursor;

		  FETCH NEXT FROM myCursor INTO @hourPart, @minutePart;
		
		  WHILE @@FETCH_STATUS = 0 
		  BEGIN
        
		    --onderstaande select is voor test-doeleinden, zodat we kunnen zien hoe SP_AddPlannerDetail aangeroepen wordt
		    /*
		 	SELECT 'EXEC pln.SP_AddPlannerDetail' as command, @Guid_Planner as guid_planner, @GUID_Object as guid_Object, @PlannerDateStart as startTime, 
	  		       @hourPart as hourPart, @minutePart as minutePart, @Guid_Relation as guid_relation from com.Administration where AdministrationNumber = 1;
  			*/

			EXECUTE pln.SP_EditPlannerDetail 
			        @Action,
				    @Guid_Planner, 
				    @GUID_Object,
				    @PlannerDateStart,
					@PlannerDateEnd,
				    @hourPart,
				    @minutePart,
				    @Guid_Relation,
					@GUID_ReservationIn,
					@GUID_ReservationOut output;

		    --select 'Main' , @GUID_ReservationIn as GUID_In, @GUID_ReservationOut as GUID_Out;

			-- Teruggekomen GUID gebruiken als input
			if @GUID_ReservationIN IS NULL set @GUID_ReservationIn = @GUID_ReservationOut;

			FETCH NEXT FROM myCursor INTO @hourPart, @minutePart;
		  END;
		  
		  CLOSE myCursor;
		  DEALLOCATE myCursor;
		  
		end try
		begin catch
		
		  if @@TRANCOUNT > 0 
		  begin
		  
		    ROLLBACK TRANSACTION;

			exec [pln].[usp_ErrorLog] 
			  @AdditionalInfo = 'Boeking niet gemaakt';

          end;

		end catch;
       
	    if @@TRANCOUNT > 0 
		begin
		
		  COMMIT TRANSACTION;
		  
		  set @Result         = 1; 
		  set @AdditionalInfo = 'Boeking is succesvol gemaakt'; 

    
		end;
	END			-- if @TimeLineInterval <> '00:00'
	else begin

	 set @Result         = 0;
	 set @AdditionalInfo = 'Er is geen geschikt tijdsinterval gevonden';
	 
	 exec [pln].[usp_ErrorLog] 
		@AdditionalInfo =@AdditionalInfo ;

	end;
END

select @Result as result, @AdditionalInfo as additionalInfo;

GO

/* SP_AddPlanner, een entry aanmaken in de planner


  Krijgt de volgende parameters binnen

  - GUID van de planner
  - Object  (bijv. baan 1)
  - Datum
  - Uur     (1 t/m 24)
  - Tijdvak (1 t/m 12)
  - GUID_Relation

*/

CREATE PROCEDURE [pln].[SP_EditPlannerDetail]
    @Action					varchar(20),
    @GUID_Planner			uniqueidentifier,
	@GUID_object			uniqueidentifier,
	@PlannerDateStart		datetime,
	@PlannerDateEnd			datetime,
	@PlannerHour			int,
	@PlannerTimePeriod		int,
	@GUID_Relation  		uniqueidentifier,
	@GUID_ReservationIN		uniqueidentifier,
	@GUID_ReservationOUT    uniqueidentifier output
AS
BEGIN
	
   DECLARE @GUID_Administration uniqueidentifier = (SELECT P.GUID_administration FROM pln.Planner P WHERE P.GUID = @GUID_Planner);
 
   declare @GUID_PlannerDetail uniqueidentifier;

   declare @ReservationNumber int; 
   declare @ReservationCode varchar(30);
   declare @ShortNameAccommodation varchar(3) = (select A.Shortname from [pln].[Accommodation] A 
                                                 inner join [pln].[Planner] P on P.GUID = @GUID_Planner
												 where A.GUID = P.GUID_Accommodation);
   
   set @GUID_PlannerDetail = (SELECT PD.GUID 
                               FROM [pln].[PlannerDetail] PD 
							  WHERE PD.GUID_Planner = @GUID_Planner and PD.GUID_Object = @GUID_Object and PD.PlannerDate = @PlannerDateStart
								and PD.PLannerHour = @PlannerHour and PD.PlannerTimeperiod = @PlannerTimePeriod);

   --select @Action, @GUID_PlannerDetail;
   
   
   -- Verwijderen  (Bij verplaatsen is de volgorde DEL(ETE) -> ADD)
   if substring(@Action,1,3) = 'DEL' or @Action = 'MOVE'
   begin

     EXECUTE pln.SP_EditPlannerDetailDelete 
             @GUID_PlannerDetail,
	         @Guid_Planner, 
		     @GUID_Object,
		     @PlannerDateStart,
		     @PLannerHour,
		     @PlannerTimePeriod,
		     @Guid_Relation,
		     @GUID_ReservationIn,
		     @GUID_ReservationOut output;
	
   end;			-- if @Action = 'DEL' 

   if @Action = 'ADD' or @Action = 'MOVE'
   begin

     EXECUTE pln.SP_EditPlannerDetailAdd 
             @GUID_PlannerDetail,
	         @Guid_Planner, 
		     @GUID_Object,
		     @PlannerDateStart,
			 @PlannerDateEnd,
		     @PLannerHour,
		     @PlannerTimePeriod,
		     @Guid_Relation,
		     @GUID_ReservationIn,
		     @GUID_ReservationOut output;

  end;

END;

GO

/* SP_EditPlannerDetailAdd, een entry aanmaken in de planner



*/

CREATE PROCEDURE [pln].[SP_EditPlannerDetailAdd]
    @GUID_PlannerDetail     uniqueidentifier,
    @GUID_Planner			uniqueidentifier,
	@GUID_Object			uniqueidentifier,
	@PlannerDateStart		datetime,
	@PlannerDateEnd			datetime,
	@PlannerHour			int,
	@PlannerTimePeriod		int,
	@GUID_Relation  		uniqueidentifier,
	@GUID_ReservationIN		uniqueidentifier,
	@GUID_ReservationOUT    uniqueidentifier output
AS
BEGIN
	
   DECLARE @GUID_Administration uniqueidentifier = (SELECT P.GUID_administration FROM pln.Planner P WHERE P.GUID = @GUID_Planner);
 
   declare @ReservationNumber int; 
   declare @ReservationCode varchar(30);
   declare @ShortNameAccommodation varchar(3) = (select A.Shortname from [pln].[Accommodation] A 
                                                 inner join [pln].[Planner] P on P.GUID = @GUID_Planner
												 where A.GUID = P.GUID_Accommodation);
   begin try
   
     if @GUID_PlannerDetail is NULL 
     begin

	   set @GUID_PlannerDetail = newid();

	   insert [pln].[PlannerDetail] (GUID, GUID_Planner, GUID_Object, PLannerDate, PlannerHour, PlannerTimePeriod,NumberOfReservations, GUID_Administration)
		 values (@GUID_PlannerDetail, @GUID_Planner, @GUID_Object, @PlannerDateStart, @PlannerHour, @plannerTimePeriod,1,@GUID_Administration);

	 end
	 else
	 begin

	   update  [pln].[PlannerDetail]
		 set NumberOfReservations = NumberOfReservations + 1
		 where GUID = @GUID_PlannerDetail;

	 end;
   
	 -- Aanmaken van de reservering (moet later aparte SP worden)
	 if @GUID_ReservationIN IS NULL 
	 begin
	   
	   if not exists(select top 1 R.GUID from RES.Reservation R where R.GUID_Administration = @GUID_Administration)
	   begin

	     set @ReservationNumber = cast(cast(datepart(year, getdate()) as varchar(4)) + '000001' as integer);

	   end
	   else begin
   
	     set @ReservationNumber = (select coalesce(max(R.ReservationNumber),0) + 1 from res.Reservation R where R.GUID_Administration = @GUID_Administration);

	   end;
   
	   set @GUID_ReservationIn = newid();
	   set @ReservationCode  = @ShortNameAccommodation + cast(@ReservationNumber as varchar);
     
  	   --select @GUID_Reservation, @PlannerDateStart, @GUID_Administration, @ReservationNumber, @ReservationCode, @GUID_Relation;

	   insert [res].[Reservation] (GUID, ReservationDate, GUID_Administration, ReservationNumber, ReservationCode, GUID_Relation, ReservationDateEnd)
	     values (@GUID_ReservationIn, @PlannerDateStart, @GUID_Administration, @ReservationNumber, @ReservationCode, @GUID_Relation, @PlannerDateEnd);

     end;		-- if @GUID_ReservationIN IS NULL 

	 set @GUID_ReservationOut = @GUID_ReservationIn;

	 -- Aanmaken van de koppeling naar de planner
	 insert [pln].[PlannerDetailReservation] (GUID, GUID_PlannerDetail, GUID_Reservation, GUID_Administration)
	   values (newid(), @GUID_PlannerDetail, @GUID_ReservationIn, @GUID_Administration);
	
     --select 'Detail' , @GUID_ReservationIn as GUID_In, @GUID_ReservationOut as GUID_Out;

   end try			-- if @Action = 'ADD' 
   begin catch

     exec [pln].[usp_ErrorLog] 
		@AdditionalInfo = 'Het toevoegen van een boeking is niet gelukt';


   end catch;
 
END;

GO

/* SP_EditPlannerDetailAdd, een entry aanmaken in de planner



*/

CREATE PROCEDURE [pln].[SP_EditPlannerDetailDelete]
    @GUID_PlannerDetail     uniqueidentifier,
    @GUID_Planner			uniqueidentifier,
	@GUID_Object			uniqueidentifier,
	@PlannerDate			datetime,
	@PlannerHour			int,
	@PlannerTimePeriod		int,
	@GUID_Relation  		uniqueidentifier,
	@GUID_ReservationIN		uniqueidentifier,
	@GUID_ReservationOUT    uniqueidentifier output
AS
BEGIN
	
   DECLARE @GUID_Administration uniqueidentifier = (SELECT P.GUID_administration FROM pln.Planner P WHERE P.GUID = @GUID_Planner);
    
   if @GUID_PlannerDetail is NOT NULL 
   begin try
      
	 update [pln].[PlannerDetail]
	    set NumberOfReservations = NumberOfReservations - 1
	  where GUID = @GUID_PlannerDetail; 

	 -- Verwijder de (gevonden) reservering
	 if @GUID_ReservationIN is NOT NULL
	 begin try

	   -- Verwijder de koppeling tussen de reservering en de planner
	   delete from [pln].[PlannerDetailReservation] 
	    where GUID_Administration = @GUID_Administration
		  and GUID_PlannerDetail  = @GUID_PlannerDetail
		  and GUID_Reservation    = @GUID_ReservationIn;

       -- Verwijder de reservering
       delete from [res].[Reservation] 
	   where GUID_Administration = @GUID_Administration
		 and GUID                = @GUID_ReservationIN;
		
	   -- Leg deze GUID vast als output parameter
  	   set @GUID_ReservationOut = @GUID_ReservationIN;
                                                      
     end try
	 begin catch
	    
	   exec [pln].[usp_ErrorLog]
		    @AdditionalInfo = 'Verwijderen van de reservering is niet gelukt';

	 end catch;
   end try
   begin catch

     exec [pln].[usp_ErrorLog] 
		@AdditionalInfo = 'Het verwijderen van een boeking is niet gelukt';


   end catch;
 
END;

GO


/****** Object:  StoredProcedure [pln].[SP_EditReservation]    Script Date: 30-1-2018 16:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






/* SP_AddPlanner, een entry aanmaken in de planner

  Parameters

  - GUID van de planner
  - Object  (bijv. baan 1)
  - Datum start
  - Datum Einde
  - GUID_Reservation

  Het tijdvak Datum Start tot Datum Einde wordt vertaald naar Datum Uur tijdvak
  en roept daarna sp_aaddPlannerDetail aan

	2017-10-31  19:00
	2017-11-01  02:30				- sluitingstijd PLanner = middernacht

  wordt:
  
	2017-10-31  19:00
	2017-10-31  19:45
	2017-10-31  20:30
	2017-10-31  21:15
	2017-10-31  22:00
	2017-10-31  22:45
	2017-10-31  23:30				<-- wordt weggegooid 
	2017-11-01  00:30				<-- wordt weggegooid 
    2017-11-01  01:15				<-- wordt weggegooid 
    2017-11-01  02:00				<-- wordt weggegooid 

   TEST GUID_BATCH = 'ECCA36D8-9DC6-4202-8921-00AC732D92B6'
*/

ALTER PROCEDURE [pln].[SP_EditReservation]
    @GUID_Batch         uniqueidentifier


AS
BEGIN
  
  DECLARE @AdministrationNumber int   = cast(com.GetParameterValue(@Guid_Batch,N'Administration')	as int);
  DECLARE @Action varchar(20)  = cast(com.GetParameterValue(@Guid_Batch,N'Action')	as varchar(20));
  DECLARE @AccommodationShortName varchar(3) = cast(com.GetParameterValue(@Guid_Batch,'AccommodationShortName')	as varchar(3));
  DECLARE @ObjectNumberStart int = cast(com.GetParameterValue(@Guid_Batch,'ObjectNumberStart')	as int);
  DECLARE @ObjectNumberEnd int = cast(com.GetParameterValue(@Guid_Batch,'ObjectNumberEnd')	as int);
  DECLARE @DateStartTime datetime = cast(com.GetParameterValue(@Guid_Batch,'DateStartTime')	as datetime);
  DECLARE @DateEndTime datetime = cast(com.GetParameterValue(@Guid_Batch,'DateEndTime')	as datetime);
  DECLARE @RelationCodeBooker int = cast(com.GetParameterValue(@Guid_Batch,'RelationCodeBooker') as int);
  DECLARE @RelationCodeAttendee int = cast(com.GetParameterValue(@Guid_Batch,'RelationCodeAttendee') as int);
  DECLARE @ReservationNumber int = cast(com.GetParameterValue(@Guid_Batch,'ReservationNumber')	as int);

  DECLARE @GUID_Administration    uniqueidentifier = (select A.GUID  from com.Administration A where A.AdministrationNumber = @AdministrationNumber);
  DECLARE @GUID_Accommodation     uniqueidentifier = (select A.GUID  from pln.Accommodation A where A.ShortName = @AccommodationShortName);
  DECLARE @GUID_ObjectStart       uniqueidentifier = (select O.GUID  from pln.Object O where O.GUID_Accommodation = @GUID_Accommodation and O.ObjectNumber = @ObjectNumberStart);
  DECLARE @GUID_ObjectEnd         uniqueidentifier = (select O.GUID  from pln.Object O where O.GUID_Accommodation = @GUID_Accommodation and O.ObjectNumber = @ObjectNumberEnd);
  DECLARE @GUID_RelationBooker    uniqueidentifier = (select RB.GUID from rel.Relation RB where RB.RelationCode = @RelationCodeBooker);
  DECLARE @GUID_RelationAttendee  uniqueidentifier = (select RA.GUID from rel.Relation RA where RA.RelationCode = @RelationCodeAttendee);
  DECLARE @GUID_ReservationIn     uniqueidentifier = (select R.GUID  from res.Reservation R where R.ReservationNumber = @ReservationNumber);
   
  --Bepaal aan de hand van de meegestuurde GUID_Planner de GUID_Administration en de GUID_Accommodation
  DECLARE @GUID_PLanner  uniqueidentifier = (SELECT P.GUID FROM pln.Planner P where P.GUID_Accommodation = @GUID_Accommodation);
  DECLARE @GUID_Object   uniqueidentifier;
  DECLARE @GUID_ReservationOut uniqueidentifier;

  DECLARE @DurationMinutes     smallint = datediff(mi, @DateStartTime, @DateEndTime);
	
  DECLARE @StartTimeBooking	 time = cast(@DateStartTime as time),
	      @EndTimeBooking      time = cast(@DateEndTime   as time); 

  DECLARE @StartTimeRange      time, @EndTimeRange   time, @TimeLineInterval time;
  DECLARE @Minute              smallint;

  DECLARE @Result              smallint;
  DECLARE @AdditionalInfo      varchar(max);

  DECLARE @ObjectCounter        smallint;
  
  /*
  select @GUID_Administration, @GUID_Accommodation, @GUID_Planner, @ObjectNumberStart, @GUID_ObjectStart, @ObjectNumberEnd,
           @GUID_ObjectEnd, @GUID_RelationBooker, @GUID_RelationAttendee, @GUID_ReservationIn;
  */

  -- Find the Starting time, the ending time and interval for the given object	
  -- EnumerationIndex 1 = Openingstimes, 2-(bookable) Block times, 3- Closed

  -- Drop de benodige temp-table als die (nog) bestaat
  iF OBJECT_ID('tempdb..##TimeRange') IS NOT NULL DROP TABLE ##TimeRange;

  create table ##TimeRange(
	    ObjectNumber int,
		StartTime time,
		EndTime time,
		EnumerationIndex smallint,
		TimeLineInterval time,
		BookingInterval time,
		Marked bit);
  
   WITH OpeningTimes AS (
		SELECT O.ObjectNumber, ATTI.StartTime, ATTI.EndTime, ATTI.EnumerationIndex, ATTI.TimeLineInterval, ATTI.BookingInterval
		FROM       [pln].[Accommodation] A
		inner join [pln].[AccommodationTemplate] AT					on AT.GUID_Accommodation = A.GUID
		inner join [pln].[AccommodationCalendar] AC					on AC.GUID_AccommodationTemplate = AT.GUID
		inner join [pln].[AccommodationTemplateTimeInterval] ATTI	on ATTI.GUID_AccommodationTemplate = AT.GUID
		inner join [com].[Enumeration] E                            on E.GUID = ATTI.GUID_Enumeration
		inner join [com].[EnumerationList] EL                       on EL.GUID_Enumeration = E.GUID and EL.EnumerationIndex = ATTI.EnumerationIndex
		inner join [pln].[ObjectGrouping] OG                        on OG.GUID = ATTI.Guid_ObjectGrouping
		inner join [pln].[ObjectGroupingObject] OGO                 on OGO.GUID_ObjectGrouping = OG.GUID
		inner join [pln].[Object] O                                 on O.GUID = OGO.GUID_Object
		WHERE  A.GUID    = @GUID_Accommodation
		  and CONVERT(date, AC.CalendarDate) = CONVERT(date, @DateStartTime)
		  and EL.EnumerationIndex < 4
		  and O.ObjectNumber >= @ObjectNumberStart
		  and O.ObjectNumber <= @ObjectNumberEnd
	)
  INSERT INTO ##TimeRange (ObjectNumber, StartTime,EndTime, EnumerationIndex, TimeLineInterval, BookingInterval)
    SELECT ObjectNumber, StartTime,EndTime, EnumerationIndex, TimeLineInterval, BookingInterval 
	  FROM OpeningTimes;
   
  --select * from ##TimeRange;
  
  -- If the above query resolves in no records, try to find the default template
  if not exists(SELECT TOP 1 StartTime FROM ##TimeRange)
  BEGIN
	WITH DefaultOpeningTimes AS (
	SELECT ATTI.StartTime, ATTI.EndTime, ATTI.EnumerationIndex, ATTI.TimeLineInterval, ATTI.BookingInterval
	  FROM [pln].[Accommodation] A
	 inner join [pln].[AccommodationTemplate] AT					on AT.GUID_Accommodation = A.GUID
	 inner join [pln].[AccommodationTemplateTimeInterval] ATTI	on ATTI.GUID_AccommodationTemplate = AT.GUID
	 WHERE  A.GUID     = @GUID_Accommodation
	   and AT.TemplateName = 'Standaard'
	)
	INSERT INTO ##TimeRange (StartTime,EndTime, EnumerationIndex, TimeLineInterval, BookingInterval)
		SELECT StartTime,EndTime, EnumerationIndex, TimeLineInterval, BookingInterval 
		FROM DefaultOpeningTimes;
  END;

  -- De dataset met openingstijden, geblokte tijden en sluitingstijden per object
  SELECT @StartTimeRange   = StartTime,
         @EndTimeRange     = EndTime,
	     @TimeLineInterval = TimeLineInterval 
  FROM  ##TimeRange 
  WHERE DATEDIFF(SECOND, @StartTimeBooking , StartTime) <= 0
	and DATEDIFF(SECOND, @EndTimeBooking, EndTime) >= 0 

  --select @TimeLineInterval;

  -- Is er nu een tijdvak waarbinnen de gewenste boeking kan worden gemaakt
  IF @TimeLineInterval <> '00:00'
  BEGIN

    -- Gebruik onderstaande declaratie om het deel eronder te tesetn
	--declare @TimeLineInterval time = '00:15';
     
	--  Splits de tijdvakken in hun tijdsinterval
	-- Drop de benodige temp-table als die (nog) bestaat
	IF OBJECT_ID('tempdb..##TimeInterval') IS NOT NULL DROP TABLE ##TimeInterval;

	CREATE TABLE ##TimeInterval(
			Number int,
			HourPart int,
			MinutePart int,
			StartTime time,
			EndTime time,
			EnumerationIndex int,
			Marked bit
		);

	-- strip het aantal minuten uit het interval, bijv. 15
	set @Minute = DATEPART(minute, @TimeLineInterval);

	WITH CTEGenerate(Number) AS (
	SELECT 0 AS Number
	UNION ALL
	SELECT Number + 1 FROM CTEGenerate where Number < (1440 / @Minute)
	)
     
	INSERT INTO ##TimeInterval(Number) SELECT Number FROM CTEGenerate OPTION(maxrecursion 1440);

	UPDATE ##TimeInterval
	   SET StartTime  = CONVERT(time(0), DATEADD(MINUTE, Number * @Minute, '0:00'))
	 WHERE Number < (1440 / @Minute);
     
	DELETE FROM ##TimeInterval
	      WHERE StartTime  < @StartTimeBooking
		     or StartTime >= @EndTimeBooking
		     or StartTime IS NULL;

	UPDATE ##TimeInterval
	   SET HourPart   = DATEPART(HOUR, ##TimeInterval.StartTime),
		   MinutePart = (Number % (60 / @minute)) + 1;

	/*
	-- Na 1x draaien kan je de EXEC los testen door onderstaande variablen even te unmarken 
	-- en samen te draaien met de select eronder
	declare @aGuid_Planner		uniqueidentifier = 'B8B858D0-625C-41DC-87EC-D7AB4262EB15';      -- SquashPlanner
	declare @aGUID_Object		uniqueidentifier = '7D010F16-A15B-48B9-9838-8A2C85B4A289';		-- Baan 2 (leden)
	declare @aGuid_Reservation	uniqueidentifier = 'B8B858D0-625C-41DC-87EC-D7AB4262EB15';      -- Onzin GUID
	declare @aPlannerDateStart	datetime         = '2017-11-17 10:00:00';
	*/

	-- De tijdset uitgesplitst per tijdsinterval
	--select * from ##TimeInterval;

	--Zoek nu voor ieder interval of deze niet in een gesloten tijdvak valt

	-- Onderstaande SP uitvoeren voor elke ObjectNumber
	SET @ObjectCounter = @ObjectNumberStart;

	While(@ObjectCounter <= @ObjectNumberEnd)
    begin
		
	  set @GUID_Object = (select O.GUID  from pln.Object O where O.GUID_Accommodation = @GUID_Accommodation and O.ObjectNumber = @ObjectCounter);

	  EXECUTE pln.SP_EditPlanner
		          @GUID_Batch,
                  @Action,
		          @Guid_Planner, 
    	          @GUID_Object,
		          @DateStartTime,
		          @DateEndTime,
				  @GUID_ReservationIn,
				  @GUID_ReservationOut output

      --select 'Edit_Reservation', @GUID_ReservationIn, @GUID_ReservationOut;

	  -- Teruggekomen GUID gebruiken als input
   	  if @GUID_ReservationIN IS NULL set @GUID_ReservationIn = @GUID_ReservationOut;

      SET @ObjectCounter = @ObjectCounter + 1;

    end;

	set @Result         = 1; 
	set @AdditionalInfo = 'Boeking is succesvol gemaakt'; 

  end			-- IF @TimeLineInterval <> '00:00'
  else begin

	set @Result         = 0;
	set @AdditionalInfo = 'Er is geen geschikt tijdsinterval gevonden';
	 
	exec [pln].[usp_ErrorLog] 
	  	 @AdditionalInfo =@AdditionalInfo ;

  end;
END;

select @Result as result, @AdditionalInfo as additionalInfo;

GO


/****** Object:  StoredProcedure [pln].[SP_EditPlanner]    Script Date: 30-1-2018 16:35:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [pln].[SP_EditPlanner]
    @GUID_Batch          uniqueidentifier,
	@Action				 varchar(20),
	@GUID_Planner		 uniqueidentifier,
	@GUID_Object         uniqueidentifier,
	@PlannerDateStart	 datetime,
	@PlannerDateEnd   	 datetime,
	@GUID_ReservationIn  uniqueidentifier,
	@GUID_ReservationOut uniqueidentifier output
	

AS
BEGIN
    DECLARE @AdministrationNumber int   = cast(com.GetParameterValue(@Guid_Batch,N'Administration')	as int);
	DECLARE @RelationCodeBooker int = cast(com.GetParameterValue(@Guid_Batch,'RelationCodeBooker')	as int);
	DECLARE @RelationCodeAttendee int = cast(com.GetParameterValue(@Guid_Batch,'RelationCodeAttendee')	as int);
	DECLARE @ReservationNumber int = cast(com.GetParameterValue(@Guid_Batch,'ReservationNumber')	as int);

	DECLARE @GUID_Administration    uniqueidentifier = (select A.GUID from com.Administration A where A.AdministrationNumber = @AdministrationNumber);
	DECLARE @GUID_RelationBooker    uniqueidentifier = (select RB.GUID from rel.Relation RB where RB.RelationCode = @RelationCodeBooker);
    DECLARE @GUID_RelationAttendee  uniqueidentifier = (select RA.GUID from rel.Relation RA where RA.RelationCode = @RelationCodeAttendee);
    
	--Bepaal aan de hand van de meegestuurde GUID_Planner de GUID_Administration en de GUID_Accommodation
	DECLARE @GUID_Accommodation  uniqueidentifier = (SELECT A.GUID   FROM pln.Accommodation A inner join pln.Planner P on P.GUID_Accommodation = A.GUID and P.GUID = @GUID_Planner);
	
	DECLARE @DurationMinutes     smallint = datediff(mi, @PlannerDateStart, @PlannerDateEnd);
	
	DECLARE @StartTimeBooking	 time = cast(@PlannerDateStart as time),
			@EndTimeBooking      time = cast(@PlannerDateEnd   as time); 

	DECLARE @StartTimeRange      time, @EndTimeRange   time, @TimeLineInterval time;
	DECLARE @Minute              smallint;

	DECLARE @Result              smallint;
	DECLARE @AdditionalInfo      varchar(max);

   

	BEGIN TRANSACTION;

		DECLARE @hourPart INT;
		DECLARE @minutePart INT;
		DECLARE myCursor CURSOR FORWARD_ONLY FOR SELECT HourPart, MinutePart FROM ##TimeInterval;
	     
		begin try

		  OPEN myCursor;

		  FETCH NEXT FROM myCursor INTO @hourPart, @minutePart;
		
		  WHILE @@FETCH_STATUS = 0 
		  BEGIN
        
		    --onderstaande select is voor test-doeleinden, zodat we kunnen zien hoe SP_AddPlannerDetail aangeroepen wordt
		    /*
		 	SELECT 'EXEC pln.SP_AddPlannerDetail' as command, @Guid_Planner as guid_planner, @GUID_Object as guid_Object, @PlannerDateStart as startTime, 
	  		       @hourPart as hourPart, @minutePart as minutePart, @Guid_Relation as guid_relation from com.Administration where AdministrationNumber = 1;
  			*/

			EXECUTE pln.SP_EditPlannerDetail 
			        @Action,
				    @Guid_Planner, 
				    @GUID_Object,
				    @PlannerDateStart,
					@PlannerDateEnd,
				    @hourPart,
				    @minutePart,
				    @Guid_RelationBooker,
					@GUID_RelationAttendee,
					@GUID_ReservationIn,
					@GUID_ReservationOut output;

		    -- Teruggekomen GUID gebruiken als input
			if @GUID_ReservationIN IS NULL set @GUID_ReservationIn = @GUID_ReservationOut;

			FETCH NEXT FROM myCursor INTO @hourPart, @minutePart;
		  END;
		  
		  CLOSE myCursor;
		  DEALLOCATE myCursor;
		  
		end try
		begin catch
		
		  CLOSE myCursor;
		  DEALLOCATE myCursor;

		  if @@TRANCOUNT > 0 
		  begin
		  
		    ROLLBACK TRANSACTION;

			exec [pln].[usp_ErrorLog] 
			  @AdditionalInfo = 'Boeking niet gemaakt';

          end;

		end catch;
       
	    --select @@TRANCOUNT;

	    if @@TRANCOUNT > 0 
		begin
		
		  COMMIT TRANSACTION;
    
	end;
		
	--select 'Edit_Planner' , @GUID_ReservationIn as GUID_In, @GUID_ReservationOut as GUID_Out;	
		
END;

GO


