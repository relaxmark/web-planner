DROP PROCEDURE [mail].[sp_send_dbmail]

DROP FUNCTION [mail].[get_principal_id]

DROP FUNCTION [mail].[get_principal_sid]

DROP PROCEDURE [mail].[sysmail_add_account_sp]

DROP PROCEDURE [mail].[sysmail_add_profile_sp]

DROP PROCEDURE [mail].[sysmail_add_profileaccount_sp]

DROP PROCEDURE [mail].[sysmail_create_user_credential_sp]

DROP PROCEDURE [mail].[sysmail_verify_account_sp]

DROP PROCEDURE [mail].[sysmail_verify_accountparams_sp]

DROP PROCEDURE [mail].[sysmail_verify_principal_sp]

DROP PROCEDURE [mail].[sysmail_verify_profile_sp]

DROP PROCEDURE [mail].[sysmail_add_principalprofile_sp]

DROP TABLE [mail].[sysmail_account_credential]

DROP TABLE [mail].[sysmail_mailitems]

DROP TABLE [mail].[sysmail_profileaccount]

DROP TABLE [mail].[sysmail_principalprofile]

DROP TABLE [mail].[sysmail_profile]

DROP TABLE [mail].[sysmail_server]

DROP TABLE [mail].[sysmail_servertype]

DROP TABLE [mail].[sysmail_account]

DROP SCHEMA [mail]