import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatSelect} from '@angular/material/select';
import { MatSnackBar } from '@angular/material';
import { FormControl} from '@angular/forms';
import { ViewEncapsulation } from '@angular/core';

import { Observable} from 'rxjs';
import { startWith,  map} from 'rxjs/operators';

import { OrganizationService } from './service/organization.service';
import { DialogService } from './service/dialog.service';
import { PlannerService } from './service/planner.service';
import { AdministrationService } from './service/administration.service';
import { RelationService } from './service/relation.service';

import { Administration } from './base/administration';
import { User, Relation } from './base/relation';
import { BookingColor }   from './base/booking';
import { Activity }       from './base/activity';
import { BatchService, BATCH } from './service/batch.service';

@Component({
  selector: 'dialog-toevoegen',
  templateUrl: 'dialog-toevoegen.html',
  styleUrls: [ './dialog-toevoegen.css' ],
  encapsulation: ViewEncapsulation.None,
  })

export class DialogToevoegen {
  attendeeWarning : string = '';

  selectedActivity : Activity;
  selectedInstructor : Relation = undefined;
  activities : Activity[] = [];
  instructors : Relation[] = [];

  price = undefined;
  mode = 'individual';
  slide_options = {minimum: 1, maximum: 4};

  attendees : Relation[] = [];

  relationCtrl1: FormControl;
  attendeeCtrl : FormControl;
  focusCtrl : FormControl;

  filteredRelations1: Observable<any[]>;
  filteredAttendees: Observable<any[]>;

  constructor(
    private batchService: BatchService,
    private organizationService: OrganizationService,
    private dialogService: DialogService,
    private plannerService: PlannerService,
    private relationService: RelationService,
    private administrationService: AdministrationService,
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<DialogToevoegen>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.relationCtrl1 = new FormControl();
      this.attendeeCtrl = new FormControl();
      this.focusCtrl = new FormControl();

//      this.filteredRelations1 = this.relationCtrl1.valueChanges.pipe(startWith(''), map(relation => relation ? this.filterRelation(relation) : this.relationService.relations.slice()));
      this.filteredRelations1 = this.relationCtrl1.valueChanges.pipe(startWith(''), map(relation => this.filterRelation(relation)));
      this.filteredAttendees = this.attendeeCtrl.valueChanges.pipe(startWith(''), map(relation => this.filterRelation(relation)))

      this.activities = this.data.activities;

      //DONE : add the current user to the attendees list
      for (var r = 0; r < this.relationService.relations.length; r++) {
        if (this.relationService.relations[r].code == this.user().code) {
          this.attendees.push(this.relationService.relations[r]);
        }
      }


      if (this.data.item.isGroup) {
        this.mode = 'participant'
      }

      var _activities = this.getActivities(this.mode);

      if (_activities.length == 1) {
        this.selectedActivity = _activities[0];
      }
    }

  filterRelation(name : string) {
    if (name.length >= 3) {
      return this.relationService.relations.filter(relation => relation.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }
    else {
      return []
    }
  }
  
  selectActivity(activity : Activity) {
    this.selectedActivity = activity;

    var _instructors = [];

    //console.log(this.organizationService.organizations.length + ' organisaties')
    
    for (var o = 0; o < this.organizationService.organizations.length; o++) {
      var _organization = this.organizationService.organizations[o];

      //console.log(_organization.personActivities.length + ' persoon-activiteiten')

      for (var pa = 0; pa < _organization.personActivities.length; pa++) {
        var _activity = _organization.personActivities[pa].activity;
        var _person = _organization.personActivities[pa].person;

        //console.log('ACTIVITEIT : ' + JSON.stringify(activity))
        //console.log('ACTIVITEIT : ' + JSON.stringify(_activity))
        //console.log('PERSON : ' + JSON.stringify(_person))

        if (_activity.code == activity.code)  {
          _instructors.push(_person.relation);
        }
      }
    }

    this.instructors = _instructors;

    this.setWarning();
    //console.log(this.instructors.length + ' behandelaars')
  }

  getInstructors() : Relation[] {
    var _result = [];

    for (var i=0; i < this.instructors.length; i++) {
      _result.push(this.instructors[i]);
    }

    return _result;
  }


  getActivities(type) : Activity[] {
    var _result = [];

    for (var a = 0; a < this.activities.length; a++) {
      var _activity = this.activities[a];

      if (_activity.type == this.mode) {
        _result.push(this.activities[a])
      }
    }


    return _result;
  }

  selectInstructor(instructor : Relation) {
    this.selectedInstructor = instructor;
  }

  selectAttendee(event, attendee) {
    this.attendeeWarning = "";

    if (event.source.selected) {
      if (this.attendees.indexOf(attendee) != -1) {
        this.attendeeWarning = (attendee.name + " is al toegevoegd");
      }
      else {
        this.attendees.push(attendee);

        this.setWarning();
      }

      this.attendeeCtrl.setValue('');
    }
  }

  displayNull(value) {
    return null;
  }

  deleteAttendee(attendee) {
    this.attendees.splice(this.attendees.indexOf(attendee), 1)
    this.setWarning();
  }

  showAddParticipantInfo() {
    this.dialogService.showMessage("Is de deelnemer niet bekend, kies dan 'Introducee'", "SportPlanner")
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  selectedDate() : string {
    var _date = this.plannerService.selectedDate.getDate() + ' ' + this.plannerService.selectedDate.toLocaleString('nl-nl', {month: "long"});

    if (this.plannerService.selectedDate.getFullYear() != new Date().getFullYear()) {
      _date = _date + ' ' + this.plannerService.selectedDate.getFullYear();
    }

    return _date;
  }

  selectedRooms() : string {
    var _result = '';

    if (this.data.start && this.data.start != this.data.end) {
      var _start = this.data.start.room.number;
      var _end = this.data.end.room.number;

      if (this.data.start.room.number > this.data.end.room.number) {
        _start = this.data.end.room.number;
        _end = this.data.start.room.number;
      }


      for (var r = _start; r <= _end; r++) {
        if (_result != '') {
          _result = _result + ', ';
        }

        _result = _result + r;
      }

    }
    else {
      _result = this.data.start.room.number;
    }

    return _result
  }

  allowMore() : boolean {
    var _result = false;

    _result = 
        this.user().hasRight('CreatePrimeTime') ||
        this.user().hasRight('CreateHappyHour') ||
        this.user().hasRight('CreateGroupBooking') ||
        this.user().hasRight('CreateClosing')


    return _result;
  }

  more() {
    this.setMode('menu');
  }

  setMode(mode) {
    this.mode = mode;

    //reset de lijst met activiteiten
    this.selectedActivity = undefined;
    this.activities = this.data.activities;
  }

  getModeCaption(mode) : string {
    if (mode == 'menu') { return "Maak uw keuze" }
    if (mode == 'individual') { return "Nieuwe reservering" }
    if (mode == 'discount') { return "Happy Hour" }
    if (mode == 'prime') { return "Piek Uren" }
    if (mode == 'group') { return "Groepsles" }
    if (mode == 'closure') { return "Baan sluiten" }
    if (mode == 'participant') {
      if (this.data.isParticipant) {
        return "U staat aangemeld voor deze groepsles"      
      }
      else {
        return "Deelname aan groepsles"
      }
    }
  }

  getSaveCaption(mode) : string {
    if (mode == 'participant') {
      if (this.data.isParticipant) {
        return 'Afmelden'    
      }
      else {
        return 'Inschrijven'
      }
    }
    else {
      return "Opslaan"
    }
  }

  startTime() : string {
    return this.timeToString(this.data.start.timeStart);
  }

  endTime() : string {
    return this.timeToString(this.data.end.timeEnd);
  }

  administration() : Administration {
    return this.administrationService.selectedAdministration;
  }

  relations() : Relation[] {
    return this.relationService.relations;
  }

  save() : void {
    if (this.mode == 'participant') {
      if (this.data.isParticipant) {
        this.plannerService.deleteParticipant({"administration" : this.administrationService.selectedAdministration, "reservationNumber" : this.data.start.reservation, "reservationDetailNumber" : this.data.start.reservationDetailNumber, "relationCode" : this.user().code})
          .then(result => {
            var _batch = this.batchService.evaluateBatch(result, BATCH.REST_PARTICIPANT);

            if (_batch.success) {
              this.dialogService.showMessage(_batch.message, 'Deelnemer uitgeschreven')
            }
            else {
              this.dialogService.showMessage(_batch.message, 'Onverwacht probleem')
            }

            //console.log('ADD PARTICIPANT --> ' + JSON.stringify(result))

            this.dialogRef.close();
          })
      }
      else {
        this.plannerService.addParticipant({"administration" : this.administrationService.selectedAdministration, "reservationNumber" : this.data.start.reservation, "reservationDetailNumber" : this.data.start.reservationDetailNumber, "relationCode" : this.user().code})
          .then(result => {
            var _batch = this.batchService.evaluateBatch(result, BATCH.REST_PARTICIPANT);

            if (_batch.success) {
              this.dialogService.showMessage(_batch.message, 'Deelnemer toegevoegd')
            }
            else {
              this.dialogService.showMessage(_batch.message, 'Onverwacht probleem')
            }

            this.dialogRef.close();
          });
      }
    }
    else {
      var _parameters = {
                           administration: this.administrationService.selectedAdministration,
                           accommodation: this.data.accommodation,
                           date: this.plannerService.selectedDate,
                           itemStart: this.data.start,
                           itemEnd: this.data.end,
                           relations: [this.selectedInstructor],
                           attendees: this.attendees,
                           type: this.mode,
                           minimum: this.slide_options.minimum,
                           maximum: this.slide_options.maximum,
                           activity: this.selectedActivity
                         }

      this.plannerService.addBooking(_parameters).then(result => { this.afterSave(result) });
    }
  }

  afterSave(data : any) : void {
    var message = "";

    var _batch = this.batchService.evaluateBatch(data, BATCH.REST_BOOKING);

    var result = _batch.success;
    var message = _batch.message;

    if (result) {
      if (message) {
        this.snackBar.open(message, 'ok', {duration: 2500});
      }

      this.dialogRef.close();
    }
    else {
      this.dialogService.showMessage('De boeking kon niet worden aangemaakt', 'SportPlanner');
    }
  }

  cancel() : void {
    this.dialogRef.close();
  }

  timeToString(time : Date) : string {
    return time.getHours() + ':' + (time.getMinutes()<10?'0':'') + time.getMinutes();
  }

  user() : User {
    return this.plannerService.user;
  }

  getPrice() {
    var _parameters = {
                        administration: this.administrationService.selectedAdministration,
                        accommodation : this.data.accommodation,
                        date : this.plannerService.selectedDate,
                        itemStart: this.data.start,
                        itemEnd: this.data.end,
                        relations: [this.selectedInstructor],
                        attendees: this.attendees,
                        activity: this.selectedActivity
                      };

    this.plannerService.getPrice(_parameters).then(result => { this.setPrice(result); console.log('PRICE : ' + JSON.stringify(result)) });
  }

  setPrice(data) {
    if (data) {
      this.price = JSON.stringify(data);
    }
  }

  canSave() : boolean {
    var _result = true;

    if (this.mode == 'individual') {
      if (!this.selectedActivity || this.attendees.length < this.selectedActivity.minAttendees || this.attendees.length > this.selectedActivity.maxAttendees) {
        _result = false;
      }
    }
    else if (this.mode == 'group') {
      if (!this.selectedActivity || !this.selectedInstructor) {
        _result = false;
      }
    }

    return _result;
  }

  showAttendees() : boolean {
    var _result = false;

    if (this.mode == 'individual') {
      _result = true; // altijd beschikbaar, niet pas na selecteren activiteit (this.selectedActivity != undefined); 
    }
    else if (this.mode == 'participant') {
      _result = this.plannerService.user.hasRight('CreateReservationOnBehalf')
    }

    return _result;
  }

  showCancelGroup() : boolean {
    var _result = false;

    if (this.mode=='participant' && this.data.item.reservation) {
      if (this.plannerService.user.hasRight('DeleteGroupBooking')) {
        _result = true;
      }
    }

    return _result;
  }


//bepaal of de gebruiker uit de lijst met deelnemers verwijderd mag worden
//als je een "normale" gebruiker bent, dan mag je jezelf daar niet verwijderen, aangezien de boeking voor jou gemaakt wordt
  allowDeleteParticipant(participant) : boolean {
    var _result = true;

    if (participant.code == this.plannerService.user.code) {
      if (!this.plannerService.user.hasRight('CreateReservationOnBehalf')) {
        _result = false;
      }
    }

    return _result;

  }

  setWarning() {
    var _warning = '';

    if (this.selectedActivity && this.attendees.length > this.selectedActivity.maxAttendees) {
      _warning = 'teveel deelnemers (max. ' + this.selectedActivity.maxAttendees + ')'
    }

    if (this.selectedActivity && this.attendees.length < this.selectedActivity.minAttendees) {
      _warning = 'er zijn nog niet genoeg deelnemers (' + this.selectedActivity.minAttendees + ')'
    }


    this.attendeeWarning = _warning

  }

}
