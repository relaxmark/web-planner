import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent }      from './dashboard.component';
import { PlannerComponent }        from './planner.component';
import { AdministrationComponent } from './administration.component';
import { RouterGuard }             from './guards/router.guard'

const appRoutes: Routes = [
  { path: '', component: AdministrationComponent },
  { path: 'admin', component: AdministrationComponent },
  { path: 'admin/:id', component: AdministrationComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [RouterGuard] },
  { path: 'planner/:id', component: PlannerComponent, canActivate: [RouterGuard] },
  { path: 'activate/:id', component: DashboardComponent },
  { path: 'activeren/:id', component: DashboardComponent },
  { path: 'resetpassword/:id', component: DashboardComponent },

  { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
