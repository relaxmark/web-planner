import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { ViewEncapsulation } from '@angular/core';

import { BatchService, BATCH } from './service/batch.service';
import { PlannerService } from './service/planner.service';
import { AdministrationService } from './service/administration.service';


@Component({
  selector: 'dialog-verplaatsen',
  templateUrl: 'dialog-verplaatsen.html',
  styleUrls: [ './dialog-verplaatsen.css' ],
  encapsulation: ViewEncapsulation.None,
})

export class DialogVerplaatsen {
  constructor(
    private batchService : BatchService,
    private plannerService: PlannerService,
    private administrationService: AdministrationService,
    public dialogRef: MatDialogRef<DialogVerplaatsen>,
    public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    }

  caption() : string {
    var _result = 'Boeking ';

    if (this.data.mode == 'move') {
      _result = _result + 'verplaatsen'
    }

    if (this.data.mode == 'copy') {
      _result = _result + 'herhalen'
    }

    return _result;
  }

  buttonCaption() : string {
    var _result = 'Opslaan'

    if (this.data.mode == 'move') {
      _result = 'Verplaatsen'
    }

    if (this.data.mode == 'copy') {
      _result = 'Herhalen'
    }

    return _result;
  }

  cancel() : void {
    this.dialogRef.close();
  }

  save() : void {
    if (this.data.mode == 'move') {
      this.move();
    }

    if (this.data.mode == 'copy') {
      this.copy();
    }
  }

  copy() : void {
    var _parameters = { accommodation: this.data.planner.accommodation(),
                        date: this.plannerService.selectedDate,
                        itemStart: this.data.destination,
                        itemEnd: this.data.destination
                      }
    

    this.plannerService.addBooking(_parameters).then(result => {
      //console.log(JSON.stringify(result));

      var _batch = this.batchService.evaluateBatch(result, BATCH.REST_BOOKING)

      if ( _batch.success ) {
        this.snackBar.open(_batch.message, 'ok', {duration: 2500});

        this.dialogRef.close();
        this.data.planner.getPlannerData(this.data.planner.accommodation(), false);
      }
      else {
        this.snackBar.open(_batch.message, 'FOUT', {duration: 2500});
      }
    })
  }

  move() : void {
    console.log('MOVE - ' + JSON.stringify(this.data.source))


    var _parameters = { administration : this.administrationService.selectedAdministration,
                        accommodation: this.data.planner.accommodation(),
                        date: this.plannerService.selectedDate,
                        source : this.data.source, 
                        destination : this.data.destination, 
                        itemStart: this.data.destination,
                        itemEnd: this.data.destination
                      }

    this.plannerService.moveBooking(_parameters).then(result => {

      var _batch = this.batchService.evaluateBatch(result, BATCH.REST_BOOKING)

      if (_batch.success) {
        this.snackBar.open(_batch.message, 'ok', {duration: 2500});

        this.dialogRef.close();
        this.data.planner.getPlannerData(this.data.planner.accommodation(), false);
      }
      else {
        this.snackBar.open(_batch.message, 'FOUT', {duration: 2500});
      }
    })

  }
}

