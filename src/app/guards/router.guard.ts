import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { PlannerService } from '../service/planner.service';
import { AdministrationService } from '../service/administration.service';

@Injectable()
export class RouterGuard implements CanActivate {
 
    constructor(private router: Router,
                private plannerService: PlannerService,
                private administrationService: AdministrationService) { }
 
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      var _result = true;


      if (!this.administrationService.selectedAdministration) {
        _result = false;
        this.router.navigate(['/admin'], { queryParams: { returnUrl: state.url }});
      }

      if (route.url.length) {
        var _url : string = route.url[0].path;

        if (!_url.toLowerCase().startsWith('dashboard')) {
          if (!this.plannerService.user.code) {
            _result = false;
            this.router.navigate(['/dashboard'], { queryParams: { returnUrl: state.url }})
          }
        }
      }

      return _result;
    }
}
